//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/mpu6050_i2c.h"
#include "led_output.h"

extern I2C_HandleTypeDef hi2c1;
extern int mpu_is_initialized;
int dmpIntDataReady;
int16_t receivedMPUData[7];
int xd;

void IMU_MPU6050_Init(void){
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_PWR_MGMT_1, 1<<7);//reset the whole module first
    HAL_Delay(50);    //wait for 50ms for the gyro to stable
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_PWR_MGMT_1, MPU6050_CLOCK_PLL_ZGYRO);//PLL with Z axis gyroscope reference
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_CONFIG, 0x01);        //DLPF_CFG = 1: Fs=1khz; bandwidth=42hz
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_SMPLRT_DIV, 0x01);    //500Hz sample rate ~ 2ms
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_GYRO_CONFIG, MPU6050_GYRO_FS_2000);    //Gyro full scale setting
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_CONFIG, MPU6050_ACCEL_FS_2);    //Accel full scale setting
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    //MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_INT_PIN_CFG, 1<<4);        //interrupt status bits are cleared on any read operation
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    //MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_INT_ENABLE, 1<<0);        //interupt occurs when data is ready. The interupt routine is in the receiver.c file.
    HAL_Delay(DELAY_IMU_CYCLE_WRITE);
    MPU6050_Write(MPU6050_DEFAULT_ADDRESS, MPU6050_RA_SIGNAL_PATH_RESET, 0x07);//reset gyro and accel sensor
    HAL_Delay(50); //wait for 50ms for the gyro to stable
    //dmpIntDataReady = FALSE;
    //HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    mpu_is_initialized = TRUE;
}

void MPU6050_Write(uint8_t slaveAddr, uint8_t regAddr, uint8_t data)
{
    uint8_t tmp;
    tmp = data;
    MPU6050_I2C_ByteWrite(slaveAddr,&tmp,regAddr);
}

int getMPUDataReadyStatus()
{
    return dmpIntDataReady;
}
void setMPUDataReadyStatus(int enable){
    dmpIntDataReady = enable;
}

void IMU_MPU6050_ReadRaw(int16_t *buff){
    uint8_t tmpBuffer[14];
    MPU6050_I2C_BufferRead(MPU6050_DEFAULT_ADDRESS, tmpBuffer, MPU6050_RA_ACCEL_XOUT_H, 14);
    /* Get acceleration */
    for (int i = 0; i < 3; i++)
        buff[i] = ((uint16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
    /* Get temperature */
    buff[3] = (int16_t)((tmpBuffer[6] << 8) | tmpBuffer[7]) / 340 + 37;
    /* Get Angular rate */
    for (int i = 4; i < 7; i++)
        buff[i] = ((int16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
}

void MPUInterruptHandler(void)
{
    //setMPUDataReadyStatus(TRUE);
    IMU_MPU6050_ReadRaw(receivedMPUData);
}