//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/battery_adc.h"

extern ADC_HandleTypeDef hadc3;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim12;
float battery_voltage=0;

void runBatteryLevelADC(uint32_t* variable)
{
    HAL_ADC_Start_DMA(&hadc3, variable, 1);
}

void performLowVoltageAction() {
    HAL_ADC_Stop_DMA(&hadc3);
    TIM12->CCR1 = 0;
    TIM12->CCR2 = 0;
    HAL_TIM_Encoder_Stop(&htim2, TIM_CHANNEL_ALL);
    HAL_TIM_Encoder_Stop(&htim3, TIM_CHANNEL_ALL);
    HAL_TIM_PWM_Stop(&htim12, TIM_CHANNEL_1);
    HAL_TIM_PWM_Stop(&htim12, TIM_CHANNEL_2);

    //stop uart
    //stop sheduler

}
