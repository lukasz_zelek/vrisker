//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/led_output.h"
//charakterystyczne kody
/**
 * czerwona świeci ciągle - napięcie na baterii poniżej 7V
 * czerwona miga - napięcie na baterii poniżej 6,5 V (konieczne szybkie naładowanie)
 * LED1 ścieci ciągle - zasilanie ok, program ok
 * LED1 miga - błąd krytyczny(na 90% spowodowany błędem magistali I2C)
 * LED1,2,3,4 migają 3 razy - program uruchomił się poprawnie, można rozpocząć mapowanie przyciskiem RUN
 * LED1,2,3,4 świecą stale - program uruchomił się poprawnie, mapa wczytana , można rozpocząć fastrun przyciskiem RUN
 * sekwencja LED1,4 -> LED1,3 -> LED1,4 - tryb debug przez UART, przycisk DEBUG spowoduje uruchomienie komunikacji
 * LED3 miga - aktywność BT TX
 * LED2 miga - aktywność BT RX
 *
 *
 *
 *
 * */
extern TIM_HandleTypeDef htim14;

uint8_t ledsEnabledMask;
uint8_t timer;
uint8_t ldpwms[4] = {LED2_OUTPUT_PWM/50, LED3_OUTPUT_PWM/50, LED4_OUTPUT_PWM/50, LED5_OUTPUT_PWM/50};

void ledOutputInit()
{
    HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
    TIM14->CCR1 = 0;
    HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin, GPIO_PIN_RESET);
    ledsEnabledMask = 0;
    timer = 0;
}

void ledToggle(uint16_t ledx)
{
    switch(ledx)
    {
        case 1:
            if (TIM14->CCR1 == 0)TIM14->CCR1 = LED1_OUTPUT_PWM;
            else TIM14->CCR1 = 0;
            break;
        default:
            if(ledsEnabledMask & (1 << ledx))
                ledDisable(ledx);
            else
                ledEnable(ledx);
            break;
    }
}

void ledEnable(uint16_t ledx) {
    switch(ledx)
    {
        case 1:
            TIM14->CCR1 = LED1_OUTPUT_PWM;
            break;
        default:
            ledsEnabledMask = (1<<ledx)  | ledsEnabledMask;
            break;
    }
}

void ledDisable(uint16_t ledx)
{
    switch(ledx)
    {
        case 1:
            TIM14->CCR1 = 0;
            break;
        default:
            // usunięcie bitu na pozycji ledx = 3 ledsEnabledMask: 010101
            // usuwamy 3
            // 010101 ^ 001000 -> 011101
            // 011101 & 010101 -> 010101 nie było bitu i nadal go nie ma ;-)
            // usuwamy 2
            // 010101 ^ 000100 -> 010001
            // 010001 & 010101 -> 010001 był bit i nie ma ;-)
            ledsEnabledMask = (ledsEnabledMask ^ (1 << ledx) ) & ledsEnabledMask;
            break;
    }
}

void generateLEDPWMCallback(void)
{
    timer = (timer + 1) % 20;
    //PWM PROMILES
    //PWM SIGNAL 100 %% _________-
    //PWM SIGNAL 200 %% ________--
    //PWM SIGNAL 300 %% _______---
    //PWM SIGNAL 400 %% ______----
    //PWM SIGNAL 500 %% _____-----

    if(timer == 0)
    {
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin,GPIO_PIN_RESET);
        return;
    } else //led 2 - 5
        for(int led = 2; led < 6; led++)
            if(ledsEnabledMask & (1 << led))
                if(19-timer == ldpwms[led-2])
                {
                    switch (led) {
                        case 2:
                            HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin,GPIO_PIN_SET);
                            break;
                        case 3:
                            HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin,GPIO_PIN_SET);
                            break;
                        case 4:
                            HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin,GPIO_PIN_SET);
                            break;
                        case 5:
                            HAL_GPIO_WritePin(LED5_GPIO_Port, LED5_Pin,GPIO_PIN_SET);
                            break;
                    }
                }
}