//
// Created by Łukasz Zelek on 17.03.2020.
//

#include <stdlib.h>
#include "math_library.h"
#include "stdio.h"

/* comments
 * w bibliotece do alokacji macierzy
 * masowo będziemy używać heapa,
 * do kontroli HEAPa, wykorzystamy stos wskaźników na macierze
 * żeby go po wszelkich operacjach czyścić
 * trafiają tam tylko tymczasowe macierze, które nie są wymagane w kolejnym obiegu kalmana
 * */

matrix* matrix_stack[100];
int matrix_stack_size = 0;
extern matrix *f,*h,*P,*Q,*R,*x,*z,*Wm,*Wc,*A,*Y,*X,*X2,*m1,*Y1,*P1,*Z1,*z1,*Z2,*S,*C,*K;//dostarczamy nieusuwalne miedzy cyklami zmienne
//Zmienna zbierająca error od arm_math
arm_status errors = 0;

matrix* matini(int n, int m, char c){
    float32_t* new_tab = calloc(n*m,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,m,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = c;
    return res_mat;
}

matrix* matinv(matrix* A){
    if(A->mat->numRows != A->mat->numCols)
    {
        printf("Error using matinv\n"
               "Matrix must be square.\n");
        return NULL;
    }
    int n = A->mat->numRows;
    float32_t* new_tab = calloc(n*n,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,n,new_tab);
    errors+=arm_mat_inverse_f32(A->mat,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '^';
    return res_mat;
}

matrix* mattra(matrix* A){
    int n = A->mat->numRows;
    int m = A->mat->numCols;
    float32_t* new_tab = calloc(n*m,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,m,n,new_tab);
    errors+=arm_mat_trans_f32(A->mat,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '\'';
    return res_mat;
}

matrix* matsca(matrix* A, float32_t s){
    int n = A->mat->numRows;
    int m = A->mat->numCols;
    float32_t* new_tab = calloc(n*m,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,m,new_tab);
    errors+=arm_mat_scale_f32(A->mat,s,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '$';
    return res_mat;
}

matrix* matmul(matrix* A, matrix* B){
    int an = A->mat->numRows;
    int am = A->mat->numCols;
    int bn = B->mat->numRows;
    int bm = B->mat->numCols;
    if(am != bn)
    {
        printf("Error using matmul\n"
               "Bad matrix dimensions\n"
               "Matrices should have am equal bn.\n");
        return NULL;
    }
    float32_t* new_tab = calloc(an*bm,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,an,bm,new_tab);
    errors+=arm_mat_mult_f32(A->mat,B->mat,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '*';
    return res_mat;
}

matrix* matadd(matrix* A, matrix* B){
    int an = A->mat->numRows;
    int am = A->mat->numCols;
    int bn = B->mat->numRows;
    int bm = B->mat->numCols;
    if(an != bn || am != bm)
    {
        printf("Error using matadd\n"
               "Matrices should have identical dimensions.\n");
        return NULL;
    }
    float32_t* new_tab = calloc(an*am,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,an,am,new_tab);
    errors+=arm_mat_add_f32(A->mat,B->mat,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '+';
    return res_mat;
}

matrix* matsub(matrix* A, matrix* B){
    int an = A->mat->numRows;
    int am = A->mat->numCols;
    int bn = B->mat->numRows;
    int bm = B->mat->numCols;
    if(an != bn || am != bm)
    {
        printf("Error using matadd\n"
               "Matrices should have identical dimensions.\n");
        return NULL;
    }
    float32_t* new_tab = calloc(an*am,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,an,am,new_tab);
    errors+=arm_mat_sub_f32(A->mat,B->mat,new_mat);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '-';
    return res_mat;
}

/**
 * @brief Rozkład Choleskiego macierzy 3x3
 * @param A wskaźnik na macierz wejściową.
 */
matrix* matcho(matrix *A){
    int n = A->mat->numRows;
    int m = A->mat->numCols;
    if(m != n)
    {
        printf("Error using matcho\n"
               "Matrix must be square and symetric.\n");
        return NULL;
    }
    float32_t* new_tab = calloc(n*n,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,n,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '&';
    int n_chol = n;

    for (int i = 0; i < n_chol; i++) {
        for (int j = 0; j <= i; j++) {
            float32_t sum = 0;
            if(j == i) // summation for diagonals
            {
                for (int k = 0; k < j; k++)
                    sum += powf(new_tab[j*n_chol+k], 2);
                new_tab[j*n_chol+j] = sqrtf(A->mat->pData[j*n_chol+j] - sum);
            } else {
                for (int k = 0; k < j; k++)// Evaluating L(i, j) using L(j, j)
                    sum += (new_tab[i*n_chol+k] * new_tab[j*n_chol+k]);
                new_tab[i*n_chol+j] = (A->mat->pData[i*n_chol+j] - sum) / new_tab[j*n_chol+j];
            }
        }
    }
    return res_mat;
}

matrix* matfra(matrix* A, int n_start, int n_end, int m_start, int m_end){
    //FIXME check if usage was correct
    int n = n_end - n_start;
    int m = m_end - m_start;
    float32_t* new_tab = calloc(n*m,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,m,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '#';
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            res_mat->mat->pData[i*m+j] = A->mat->pData[(i+n_start)*m+(j+m_start)];
    return res_mat;
}

matrix* matdia(matrix* v) {
    int n = (v->mat->numRows) * (v->mat->numRows);
    float32_t* new_tab = calloc(n*n,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,n,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '\\';
    for(int i = 0; i < n; i++)
        res_mat->mat->pData[i*n+i] = v->mat->pData[i];
    return res_mat;
}

/*v - column vector*/
matrix* matdup(matrix* v, int k){
    int n = (v->mat->numRows) * (v->mat->numRows);
    float32_t* new_tab = calloc(n*k,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,k,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '|';
    for(int i = 0; i < n; i++)
        for(int j = 0; j < k; j++)
            res_mat->mat->pData[i*k+j] = v->mat->pData[i];
    return res_mat;
}

matrix* matmer(matrix* A, matrix* B){
//FIXME check if usage was correct
    int n = A->mat->numRows;
    int m1 = A->mat->numCols;
    int m2 = B->mat->numCols;
    int m = m1+m2;
    float32_t* new_tab = calloc(n*m,sizeof(float32_t));
    arm_matrix_instance_f32 *new_mat = malloc(sizeof(arm_matrix_instance_f32));
    arm_mat_init_f32(new_mat,n,m,new_tab);
    matrix *res_mat = (matrix*)malloc(sizeof(matrix));
    matrix_stack[matrix_stack_size++] = res_mat;
    res_mat->mat = new_mat;
    res_mat->friendly_name = '_';
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m1; j++)
            res_mat->mat->pData[i*m+j] = A->mat->pData[i*m1+j];
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m2; j++)
            res_mat->mat->pData[m1+i*m+j] = A->mat->pData[i*m2+j];
    return res_mat;
}

/**
 * @brief Funkcja wypisująca macierz, do debugowania.
 * @param matrix : wskaźnik na wypisywaną macierz.
 */
void matprt(matrix* A){
    printf("%c[%ix%i] = \n", A->friendly_name, A->mat->numRows, A->mat->numCols);
    for(int i = 0; i < A->mat->numRows; i++)
    {
        for(int j = 0; j < A->mat->numCols; j++)
            printf("%7.2lf ", (double) A->mat->pData[i * (A->mat->numCols) + j]);
        printf("\n");
    }
}

void matfre(void)
{
    matrix* tab_of_secured_pointers[22] = {f,h,P,Q,R,x,z,Wm,Wc,A,Y,X,X2,m1,Y1,P1,Z1,z1,Z2,S,C,K};
    while(matrix_stack_size--)
    {
        int scan_count = sizeof(tab_of_secured_pointers);
        int allowed_dealloc = TRUE;
        while(scan_count--)
            if(matrix_stack[matrix_stack_size] == tab_of_secured_pointers[scan_count])
            {
                scan_count = 0;
                allowed_dealloc = FALSE;
            }
        if(!allowed_dealloc)continue;
        if(matrix_stack[matrix_stack_size]->mat->pData != NULL)
        {
            free(matrix_stack[matrix_stack_size]->mat->pData);
        }
        else
            printf("Dealocation error!\n");
        if(matrix_stack[matrix_stack_size]->mat != NULL)
        {
            free(matrix_stack[matrix_stack_size]->mat);
        }
        else
            printf("Dealocation error!\n");
        if(matrix_stack[matrix_stack_size] != NULL)
        {
            free(matrix_stack[matrix_stack_size]);
        }
        else
            printf("Dealocation error!\n");
    }
    for(int i = 0; i < sizeof(tab_of_secured_pointers); i++)
        matrix_stack[matrix_stack_size++] = tab_of_secured_pointers[i];
}