//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/eeprom_i2c.h"
#include "uart_dma.h"


extern I2C_HandleTypeDef hi2c1;

uint8_t result1, result2, result3, result4;
uint64_t write_duration;
uint64_t read_duration;
uint64_t start_time;

volatile uint32_t eeprom_cycle_counter = 0;
volatile uint8_t eeprom_data = 0xff;
uint32_t eeprom_test_counter = 1000000000;
uint32_t eeprom_test_counter2 = 0;
uint16_t eeprom_address;

void eepromMemoryDump(uint32_t timeout) {
    //zczytanie calej pamieci
    for (uint16_t page_count = 0x00; page_count < 0x200; page_count++)
        for (uint16_t word_count = 0x00; word_count < 0x40; word_count++) {
            eeprom_address = (page_count << 6) + word_count;
            while (HAL_I2C_Mem_Read_DMA(&hi2c1, EEPROM_DEV_ADDRESS, eeprom_address, 2, (uint8_t *) &eeprom_data, 1) !=
                   HAL_OK) {
                if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
                    Error_Handler();
                }
                printf("I2C Connection Error\n");
            }
            HAL_Delay(DELAY_EEPROM_CYCLE_READ);
            printf("%.1fk 0x%x: 0x%x\n", (double)(page_count * 256.0f / 0x200), eeprom_address,
                   eeprom_data);
            ledToggle(4);
            HAL_Delay(timeout);
        }
    printf("EEPROM 256k dump DONE\n");
    HAL_Delay(5000);
}

void eepromFullMemoryErase() {
    //czyszczenie pamieci
    for (uint16_t page_count = 0x00; page_count < 0x200; page_count++)
        for (uint16_t word_count = 0x00; word_count < 0x40; word_count++) {
            eeprom_address = (page_count << 6) + word_count;
            while (HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEV_ADDRESS, eeprom_address, 2, (uint8_t *) &eeprom_data, 1,EEPROM_I2C_TIMEOUT) !=
                   HAL_OK) {
                if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
                    Error_Handler();
                }
                HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
            }
            HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
            printf("%.1lfk\n", (double)(page_count * 256.0 / 0x200));
        }
    printf("EEPROM 256k erase DONE\n");
    HAL_Delay(5000);
}

uint32_t eepromRead4Bytes(uint16_t addr) {

    while (HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEV_ADDRESS, addr, 2, (uint8_t *) &result1, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    }
    HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    while (HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEV_ADDRESS, addr + 1, 2, (uint8_t *) &result2, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    }
    HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    while (HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEV_ADDRESS, addr + 2, 2, (uint8_t *) &result3, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    }
    HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    while (HAL_I2C_Mem_Read(&hi2c1, EEPROM_DEV_ADDRESS, addr + 3, 2, (uint8_t *) &result4, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    }
    HAL_Delay(DELAY_EEPROM_CYCLE_READ);
    return (result1 << 24 | result2 << 16 | result3 << 8 | result4);
}

void eepromSave4Bytes(uint16_t *addr, uint32_t *data) {
    result1 = (*data >> 24), result2 = (*data >> 16), result3 = (*data >> 8), result4 = *data;

    while (HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEV_ADDRESS, *addr, 2, (uint8_t *) &result1, 1, EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    }
    //{}else printf("I2C Connection Error1-Write\n");
    HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    while (HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEV_ADDRESS, *addr + 1, 2, (uint8_t *) &result2, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    }
    //{}else printf("I2C Connection Error2-Write\n");
    HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    while (HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEV_ADDRESS, *addr + 2, 2, (uint8_t *) &result3, 1,EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    }
    //{}else printf("I2C Connection Error3-Write\n");
    HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    while (HAL_I2C_Mem_Write(&hi2c1, EEPROM_DEV_ADDRESS, *addr + 3, 2, (uint8_t *) &result4, 1, EEPROM_I2C_TIMEOUT) != HAL_OK) {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
            Error_Handler();
        }
        HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
    }
    //{}else printf("I2C Connection Error4-Write\n");
    HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
}

void eepromMemoryTestRW() {
    start_time = HAL_GetTick();
    //zapis
    for (uint16_t page_count = 0x00; page_count < 0x200; page_count++)
        for (uint16_t word_count = 0x00; word_count < 0x40; word_count += 4) {
            eeprom_address = (page_count << 6) + word_count;
            eepromSave4Bytes(&eeprom_address, &eeprom_test_counter);
            HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
            if (page_count % 2 == 0 && word_count % 60 == 0) {
                printf("%.1lfk\n", (double)(page_count * 256.0 / 0x200));
            }
            HAL_Delay(DELAY_EEPROM_CYCLE_WRITE);
            eeprom_test_counter++;
        }
    write_duration = HAL_GetTick() - start_time;
    printf("END\n");
    //odczyt + sprawdzenie
    uint32_t i = 1000000000;
    start_time = HAL_GetTick();
    for (uint16_t page_count = 0x00; page_count < 0x200; page_count++)
        for (uint16_t word_count = 0x00; word_count < 0x40; word_count += 4) {
            eeprom_address = (page_count << 6) + word_count;
            eeprom_test_counter2 = eepromRead4Bytes(eeprom_address);
            HAL_Delay(DELAY_EEPROM_CYCLE_READ);
            if (page_count % 2 == 0 && word_count % 60 == 0) {
                printf("%.1lfk\n", (double)(page_count * 256.0 / 0x200));
            }
            HAL_Delay(DELAY_EEPROM_CYCLE_READ);
            //sprintf(uart_sending_buffer, "%d %d\n",i,eeprom_test_counter2 );
            //printf((char *) uart_sending_buffer);
            if (eeprom_test_counter2 != i) {
                printf("Bad R/W operation at %x. Expected %d. Given %d.\n", eeprom_address, i,
                       eeprom_test_counter2);
                HAL_Delay(5000);
                return;
            }
            i++;
        }
    read_duration = HAL_GetTick() - start_time;
    printf("Test passed!\n Stats:\n Write 32 kylobytes in %ld \n Read 32 kylobytes in %ld \n", write_duration,
           read_duration);
    HAL_Delay(5000);
}

void eepromMemoryTestErase() {
    for (uint16_t page_count = 0x00; page_count < 0x200; page_count++)
        for (uint16_t word_count = 0x00; word_count < 0x40; word_count++) {
            eeprom_address = (page_count << 6) + word_count;
            while (HAL_I2C_Mem_Read_DMA(&hi2c1, EEPROM_DEV_ADDRESS, eeprom_address, 2, (uint8_t *) &eeprom_data, 1) !=
                   HAL_OK)
                if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF) {
                    Error_Handler();
                }
            HAL_Delay(DELAY_EEPROM_CYCLE_READ);
            if (page_count % 2 == 0 && word_count % 63 == 0) {
                printf("%.1lfk\n", (double)(page_count * 256.0 / 0x200));
            }
            if (eeprom_data != 0xff) {
                printf("Erase operation failed at %x. Given %d.\n", eeprom_address,
                       eeprom_data);
                HAL_Delay(5000);
                return;
            }
        }
    printf("Test passed");
    HAL_Delay(5000);
}