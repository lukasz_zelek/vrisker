//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/motors_pwm.h"

int32_t actual_left_duty = 0;
int32_t actual_right_duty = 0;
extern TIM_HandleTypeDef htim12;

void motorsPWMInit()
{
    HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_2);
    TIM12->CCR1=0;
    TIM12->CCR2=0;
}

void motorLeftSetPWM(int32_t duty)
{
    if(duty*actual_left_duty <= 0)
    {
        HAL_GPIO_WritePin(AFOR_GPIO_Port,AFOR_Pin,duty >= 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
        HAL_GPIO_WritePin(ABAC_GPIO_Port,ABAC_Pin, duty >= 0 ? GPIO_PIN_SET : GPIO_PIN_RESET);
    }
    TIM12->CCR1 = duty > 0 ? duty : -duty;
    actual_left_duty = duty;
}

void motorRightSetPWM(int32_t duty)
{
    if(duty*actual_right_duty <= 0)
    {
        HAL_GPIO_WritePin(BFOR_GPIO_Port,BFOR_Pin,duty >= 0 ? GPIO_PIN_SET : GPIO_PIN_RESET);
        HAL_GPIO_WritePin(BBAC_GPIO_Port,BBAC_Pin, duty >= 0 ? GPIO_PIN_RESET : GPIO_PIN_SET);
    }
    TIM12->CCR2 = duty > 0 ? duty : -duty;
    actual_right_duty = duty;
}

int motorLeftGetPWM(){ return actual_left_duty;}
int motorRightGetPWM(){ return actual_right_duty;}

