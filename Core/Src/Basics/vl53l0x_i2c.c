//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/vl53l0x_i2c.h"
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"
#include "mpu6050_i2c.h"
#include "led_output.h"

extern I2C_HandleTypeDef hi2c1;
extern int dmaI2CDataReceived;
extern int dmaI2CDataTransmited;

VL53L0X_RangingMeasurementData_t RangingData[5];
VL53L0X_Dev_t Dev[5];
volatile int TOF_DATA_READ[5]={FALSE, FALSE, FALSE, FALSE, FALSE};
volatile uint16_t DIST[5]={0,0,0,0,0};
int vls_initialized = FALSE;
/**
  * @brief  The multiple VL53L0X initialisation
  * @retval void
  */
void VL53L0X_Init()
{
    uint16_t Id;
    VL53L0X_DisableInterrupts();
    VL53L0X_DeviceError status = 0;
    VL53L0X_ResetAllSensors();
    HAL_Delay(10);
    for (int i = 0; i < 5; i++)
    {
        VL53L0X_SensorTurnOn(i);
        Dev[i].I2cHandle = &hi2c1;
        Dev[i].I2cDevAddr = 0x52;
        Dev[i].Present = 0;
        Dev[i].Id = 1;
        Dev[i].DevLetter = 'a';
        HAL_Delay(2);
        uint8_t FinalAddress = Dev[i].I2cDevAddr + 2*(i+1);
        do {
            /* Set I2C standard mode (400 KHz) before doing the first register access */
            if (status == VL53L0X_ERROR_NONE)
                status = VL53L0X_WrByte(&Dev[i],0x88, 0x00);
            /* Try to read one register using default 0x52 address */
            status = VL53L0X_RdWord(&Dev[i], VL53L0X_REG_IDENTIFICATION_MODEL_ID, &Id);
            HAL_Delay(2);
            if (status) {
                printf("#%d Read id fail\n", i);
                break;
            }
            if (Id == 0xEEAA) {
                /* Sensor is found => Change its I2C address to final one */
                status = VL53L0X_SetDeviceAddress(&Dev[i],FinalAddress);
                HAL_Delay(2);
                if (status != 0) {
                    printf("#i VL53L0X_SetDeviceAddress fail\n", i);
                    break;
                }
                Dev[i].I2cDevAddr = FinalAddress;
                /* Check all is OK with the new I2C address and initialize the sensor */
                status = VL53L0X_RdWord(&Dev[i], VL53L0X_REG_IDENTIFICATION_MODEL_ID, &Id);
                HAL_Delay(2);
                if (status != 0) {
                    printf("#i VL53L0X_RdWord fail\n", i);
                    break;
                }
                status = VL53L0X_DataInit(&Dev[i]);
                HAL_Delay(2);
                if( status == 0 ){
                    Dev[i].Present = 1;
                }
                else{
                    printf("VL53L0X_DataInit %d fail\n", i);
                    break;
                }
                Dev[i].Present = 1;
            }
            else {
                printf("#%d unknown ID %x\n", i, Id);
                status = 1;
            }
        } while (0);
        HAL_Delay(10);
        VL53L0X_InitContinuous(i);
        HAL_Delay(2);
    }
    HAL_Delay(10);
    VL53L0X_EnableInterrupts();
    HAL_Delay(2);
    printf(!status ? "VL53L0X All Chips booted correctly\n" : "VL53L0X Booting error\n");
    //vls_initialized = TRUE;
}


void VL53L0X_ResetAllSensors(){
    HAL_GPIO_WritePin(XSHUT1_GPIO_Port,XSHUT1_Pin,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(XSHUT2_GPIO_Port,XSHUT2_Pin,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(XSHUT3_GPIO_Port,XSHUT3_Pin,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(XSHUT4_GPIO_Port,XSHUT4_Pin,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(XSHUT5_GPIO_Port,XSHUT5_Pin,GPIO_PIN_RESET);
}

void VL53L0X_SensorTurnOn(int sens)
{
    switch(sens)
    {
        case 0:
            HAL_GPIO_WritePin(XSHUT1_GPIO_Port,XSHUT1_Pin,GPIO_PIN_SET);
            break;
        case 1:
            HAL_GPIO_WritePin(XSHUT2_GPIO_Port,XSHUT2_Pin,GPIO_PIN_SET);
            break;
        case 2:
            HAL_GPIO_WritePin(XSHUT3_GPIO_Port,XSHUT3_Pin,GPIO_PIN_SET);
            break;
        case 3:
            HAL_GPIO_WritePin(XSHUT4_GPIO_Port,XSHUT4_Pin,GPIO_PIN_SET);
            break;
        case 4:
            HAL_GPIO_WritePin(XSHUT5_GPIO_Port,XSHUT5_Pin,GPIO_PIN_SET);
            break;
    }
}
void VL53L0X_DisableInterrupts()
{
    HAL_NVIC_DisableIRQ(EXTI3_IRQn);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
}

void VL53L0X_EnableInterrupts()
{

    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

/**
  * @brief  The VL53L0X initialisation function in continuous interrupt ranging mode
  * @retval void
  */
void VL53L0X_InitContinuous(int s){
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;

    VL53L0X_WaitDeviceBooted( &Dev[s]);
    VL53L0X_DataInit( &Dev[s]);
    VL53L0X_StaticInit( &Dev[s] );
    VL53L0X_PerformRefCalibration(&Dev[s], &VhvSettings, &PhaseCal);
    VL53L0X_PerformRefSpadManagement(&Dev[s], &refSpadCount, &isApertureSpads);
    VL53L0X_SetDeviceMode(&Dev[s], VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
    VL53L0X_StartMeasurement(&Dev[s]);
}

/**
  * @brief  Function performing continuous distance measurement.
  * @retval int
  */
uint16_t VL53L0X_ContinuousRequest(int s)
{
    if(TOF_DATA_READ[s] == TRUE)
    {
        TOF_DATA_READ[s] = FALSE;
        return RangingData[s].RangeMilliMeter;
    }
    else return -1;
}
void VL53L0X_PerformMeasurement(int sensor)
{
    VL53L0X_GetRangingMeasurementData(&Dev[sensor], &RangingData[sensor]);
    VL53L0X_ClearInterruptMask(&Dev[sensor], VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
    DIST[sensor]=RangingData[sensor].RangeMilliMeter;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
    switch (GPIO_Pin)
    {
        case INT1_Pin:
            TOF_DATA_READ[0] = TRUE;
            VL53L0X_PerformMeasurement(0);
           // ledToggle(1);
            break;
        case INT2_Pin:
            TOF_DATA_READ[1]= TRUE;
           VL53L0X_PerformMeasurement(1);
           // ledToggle(2);
            break;
        case INT3_Pin:
            TOF_DATA_READ[2] = TRUE;
            VL53L0X_PerformMeasurement(2);
          //  ledToggle(3);
            break;
        case INT4_Pin:
            TOF_DATA_READ[3] = TRUE;
            VL53L0X_PerformMeasurement(3);
          //  ledToggle(4);
            break;
        case INT5_Pin:
            TOF_DATA_READ[4] = TRUE;
            VL53L0X_PerformMeasurement(4);
          //  ledToggle(5);
            break;
        case MPUINT_Pin:
            MPUInterruptHandler();
            break;
    }
}