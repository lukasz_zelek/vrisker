//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/encoders_odometry.h"

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

extern int32_t actual_left_duty;
extern int32_t actual_right_duty;
int last_left_set = 0, last_right_set = 0;

void encodersInit()
{
    HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
    HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
    resetLeftTickCount();
    resetRightTickCount();
}

int32_t getLeftTickCount(){
    int32_t product = (int)(TIM2->CNT)-last_left_set;
    return -(product > 32000 ? product - 65535 : product);
}
int32_t getRightTickCount(){
    int32_t product = (int)(TIM3->CNT)-last_right_set;
    return -(product > 32000 ? product - 65535 : product);
}

//uint16_t getLeftTickCount(){
//    return (actual_left_duty > 0 ? 16000 - (TIM2->CNT) : actual_left_duty == 0 ? TIM2->CNT - 16000 : TIM2->CNT - 32000);
//}
//uint16_t getRightTickCount(){
//    return (actual_right_duty > 0 ? 16000 - (TIM3->CNT) : actual_right_duty == 0 ? TIM2->CNT - 16000 : TIM3->CNT - 32000);
//}

void resetLeftTickCount(){
    if(actual_left_duty >= 0) {TIM2->CNT = 16000;last_left_set = 16000;} else {TIM2->CNT = 32000;last_left_set = 32000;}
}

void resetRightTickCount(){
    if(actual_right_duty >= 0) {TIM3->CNT = 16000;last_right_set = 16000;} else {TIM3->CNT = 32000;last_right_set = 32000;}
}

