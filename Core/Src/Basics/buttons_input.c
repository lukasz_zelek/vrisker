//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Basics/buttons_input.h"

int run_action_flag;
int debug_action_flag;

GPIO_PinState BTN_IsPressed(uint8_t btnid) {
    if (btnid == 1)
        return !HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin);
    else if (btnid == 2)
        return !HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin);
    return GPIO_PIN_RESET;
}
