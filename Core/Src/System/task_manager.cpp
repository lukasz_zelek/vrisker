//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "task_manager.hpp"
#include "main.h"
#include "battery_level.h"
#include "buttons_control.h"
#include "distance_sensors.h"
#ifdef __cplusplus
}
#endif
#include "imu_sensors.h"
#include "flood_fill.h"
#include "bellman_ford.h"
#include "dijkstra_search.h"
#include "kalman_filter.h"
#include "landmark_detection.h"
#include "eeprom_memory.h"
#include "pid_control.h"
#include "path_planner.h"
#include "uart_communication.h"
#include "eeprom_i2c.h"

extern uint8_t start_task_list[14];

void TaskManagerInit(uint32_t task_list){
    //default
    start_task_list[BATTERY_LEVEL_ID] = TRUE;
    start_task_list[BUTTONS_CONTROL_ID] = TRUE;
    start_task_list[DISTANCE_SENSORS_ID] = FALSE;
    start_task_list[IMU_SENSORS_ID] = FALSE;
    start_task_list[FLOOD_FILL_ID] = FALSE;
    start_task_list[BELLMAN_FORD_ID] = FALSE;
    start_task_list[DIJKSTRA_SEARCH_ID] = FALSE;
    start_task_list[KALMAN_FILTER_ID] = FALSE;
    start_task_list[LANDMARK_DETECTION_ID] = FALSE;
    start_task_list[EEPROM_MEMORY_ID] = FALSE;
    start_task_list[PID_CONTROL_ID] = TRUE;
    start_task_list[UART_RECEIVE_ID] = TRUE;
    start_task_list[UART_TRANSMIT_ID] = TRUE;
    start_task_list[PATH_PLANNER_ID] = FALSE;
    //DEFAULT 01110000000101

    start_task_list[BATTERY_LEVEL_ID] = (task_list&(1<<BATTERY_LEVEL_ID))>0;
    start_task_list[BUTTONS_CONTROL_ID] = (task_list&(1<<BUTTONS_CONTROL_ID))>0;
    start_task_list[DISTANCE_SENSORS_ID] = (task_list&(1<<DISTANCE_SENSORS_ID))>0;
    start_task_list[IMU_SENSORS_ID] = (task_list&(1<<IMU_SENSORS_ID))>0;
    start_task_list[FLOOD_FILL_ID] = (task_list&(1<<FLOOD_FILL_ID))>0;
    start_task_list[BELLMAN_FORD_ID] = (task_list&(1<<BELLMAN_FORD_ID))>0;
    start_task_list[DIJKSTRA_SEARCH_ID] = (task_list&(1<<DIJKSTRA_SEARCH_ID))>0;
    start_task_list[KALMAN_FILTER_ID] = (task_list&(1<<KALMAN_FILTER_ID))>0;
    start_task_list[LANDMARK_DETECTION_ID] = (task_list&(1<<LANDMARK_DETECTION_ID))>0;
    start_task_list[EEPROM_MEMORY_ID] = (task_list&(1<<EEPROM_MEMORY_ID))>0;
    start_task_list[PID_CONTROL_ID] = (task_list&(1<<PID_CONTROL_ID))>0;
    start_task_list[UART_RECEIVE_ID] = (task_list&(1<<UART_RECEIVE_ID))>0;
    start_task_list[UART_TRANSMIT_ID] = (task_list&(1<<UART_TRANSMIT_ID))>0;
    start_task_list[PATH_PLANNER_ID] = (task_list&(1<<PATH_PLANNER_ID))>0;
}
void vTaskManagerTask(void *pvParameters){}