//
// Created by Łukasz Zelek on 05.06.2020.
//
#include <main.h>
#include <vector>
//#include <string>
//#include <random>
#include <algorithm>
#include <iostream>
//#include <queue>
//#include <cstdlib>

//int N = 1000;
//int M = 1 << 12;
//std::string test_string = "Testuje dzialanie stringa na ARM\n\r";
//std::vector<std::string> tasks(N);
//std::vector<int> random_numbers(M);
//std::random_device rd;
//std::priority_queue<std::pair<int,std::string>> kolejka_priorytetowa;

//class Element{
//public:
//    Element();
//    ~Element();
//private:
//};

std::vector<int> data_to_sort;
void sort_using_std(int array[], int n)
{
    for(int i = 0; i < n; i++)
    {
        data_to_sort.push_back(array[i]);
    }
    std::sort(data_to_sort.begin(), data_to_sort.end());
    for(int i = 0; i < n; i++)
        array[i]=data_to_sort[i];
    data_to_sort.clear();
}
//void makeSomeCppWork()
//{
//    srand(rd());
//    for (int i = 0; i < N; ++i) {
//        //std::cout << test_string << std::endl;
//        tasks.push_back(test_string);
//    }
//    for(int i = 0; i < M; i++)
//        random_numbers.push_back(std::rand());
//    int m = M;
//    while(m--)
//        kolejka_priorytetowa.push(std::make_pair(random_numbers[m], test_string));
//    std::sort(random_numbers.begin(), random_numbers.end());
//    Element elem();
//}


void CppMain(){
    /*
    // todo procedura po inicjalizacji hardware
    // init 5 files
    // 0 battery_level
    // 11, 12 uart_communication (tx and rx)
    // 2 buttons_control
    // 14 task_manager
    // 5 eeprom_memory
    // create six tasks and start rtos

    // fixme next_lvl sensors with drivers
    // init 2 files
    // 7 imu_sensors, start timer it
    // 4 distance_sensors, start it
    // create two tasks

    // fixme next_lvl cyclic algo_tasks
    // init 4 files
    // 8 kalman_filter
    // 10 pid_control
    // 9 landmark_detection
    // 13 path planner
    // create four tasks with sequence ... -> landmark_detection -> kalman_filter -> path_planner -> pid_control -> ...
    // remember to use semaphores and mutexes to synchronise

    // fixme next_lvl cyclic main_logic
    // init 1 file
    // 15 main_control 1kHz
    // create one task

    // fixme next_lvl acyclic algo
    // init 3 files
    // 3 dijkstra_search (maze search)
    // 1 bellman_ford (also A* algorithm)
    // 6 flood_fill (main maze solving algorithm)
    // occasionally create and stop this three algorithms

    // pseudo polymorphism
    // 16 RTOS tasks
    // 1 FPU context
    // 2 TIM interrupts
    // 1 MPU external interrupt
    // 5 VL53L0X external interrupts
    // 1 UART interrupt
    // 1 I2C interrupt
    // 2 UARTDMA interrupts
    // 2 I2CDMA interrupts
    // 1 ADCDMA interrupt
*/
    create_tasks();
    start_rtos();
    //makeSomeCppWork();
    //while(1){}
    //TODO dodać Google Test Framework
}