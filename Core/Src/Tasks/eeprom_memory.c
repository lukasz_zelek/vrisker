//
// Created by Łukasz Zelek on 17.03.2020.
//
// przeprowadza operacje R/W na pamięci EEPROM

// na wejście podajemy typ operacji a następnie stosowne parametry w zależności od jej rodzaju
// na wyjście dostajemy potwierdzenie zapisu lub dane odczytane

#include "Tasks/eeprom_memory.h"

extern TaskID actual_task_id;

void EEPROMMemoryInit()
{
    //eepromMemoryTestRW();
}

void vEEPROMMemoryTask(void *pvParameters) {

    for(;;) {
        actual_task_id = EEPROM_MEMORY_ID;
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}
