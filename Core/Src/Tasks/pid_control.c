//
// Created by Łukasz Zelek on 17.03.2020.
//
// zawiera regulatory działające bezpośrednio na silnikach
// na wejście przydają się odchyły na prędkościach [vErr,omegaErr]
// na wyjście dostajemy obliczone korekty podawane prosto na silniki [vTurn,omegaTurn]

#include <arm_math.h>
#include "Tasks/pid_control.h"
#include "motors_pwm.h"
#include "uart_dma.h"

extern TaskID actual_task_id;

extern int transmitBufferSize;
extern int run_action_flag;
extern int debug_action_flag;

extern int przemieszczenie;
extern int kalman_flag;
int kalman_start_time = 1e9;

void PIDControlInit()
{
    motorsPWMInit();
    // fixme konfiguracja PID
    // wariantA: 2 x PID na silniki, sprzężenie zwrotne z enkoderów + 1 x PID na jazdę po trasie sprzężenie z danych z Filtru Kalmana
    // wariantB: 1 x PID na prędkość do przodu + 1 x PID na prędkość kątową
}

void vPIDControlTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 1;
    xLastWakeTime = xTaskGetTickCount();
    int counter = 0;
    for(;;) {
        counter++;
        if(counter > kalman_start_time)
        {
            motorLeftSetPWM(100);
            motorRightSetPWM(100);
        }

        if(run_action_flag)
        {
            run_action_flag = 0;
            if(!kalman_flag)kalman_start_time = counter + 3 * (1000/xFrequency);
            kalman_flag = 1;
        }
        if(przemieszczenie < 500)
        {

        } else{
            motorLeftSetPWM(0);
            motorRightSetPWM(0);
        }
        actual_task_id = PID_CONTROL_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}
