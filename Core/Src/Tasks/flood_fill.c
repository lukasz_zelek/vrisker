//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Tasks/flood_fill.h"

extern TaskID actual_task_id;

//algorytm przeszukiwania labirytntu polegający na sukcesywnym dążeniu do celu (centrum labiryntu, kwadrat 2x2 kratki)
//task_cykliczny wywoływany po osiąganiu kolejnych nowych pól
// na wejście - graf + aktualny_stan_ścianek(od landmark_detection)
// na wyjście - decyzja o dalszym ruchu enum[MOVE FORWARD, TURN LEFT 45, TURN LEFT 90, TURN RIGHT 45, TURN RIGHT 90 itd]


void FloodFillInit()
{

}

void vFloodFillTask(void *pvParameters) {

    for(;;) {
        HAL_Delay(20);
        actual_task_id = FLOOD_FILL_ID;
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}
