//
// Created by Łukasz Zelek on 24.03.2020.
//
// poniższy kod obsługuje zdarzenia wejściowe od przycisków

#include "Tasks/buttons_control.h"
#include "uart_dma.h"

extern TaskID actual_task_id;
extern int run_action_flag;
extern int debug_action_flag;
/**
 * @brief Inicjalizacja flag odpowiedzialnych za śledzenie przycisków.
 * @returns none
 */
void ButtonsControlInit()
{
    run_action_flag = FALSE;
    debug_action_flag = FALSE;
}

/**
 * @brief Zadanie FreeRTOS wykonujące skanowanie i aktualizację flag przycisków.
 * @param pvParameters
 * @returns none
 */
void vButtonsControlTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 50;
    xLastWakeTime = xTaskGetTickCount();
    int latest_raf=0, latest_daf=0;
    for(;;) {
        if(!run_action_flag)
            run_action_flag = BTN_IsPressed(1);
        if(!debug_action_flag)
            debug_action_flag = BTN_IsPressed(2);

        if(!latest_raf*run_action_flag){}

        if(!latest_daf*debug_action_flag){}

        latest_raf = run_action_flag;
        latest_daf = debug_action_flag;
        actual_task_id = BUTTONS_CONTROL_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}