//
// Created by Łukasz Zelek on 17.03.2020.
//

#include "Tasks/dijkstra_search.h"

// algorytm znajdowania najkrótszej ścieżki w grafie O(E log V)
// trzeba użyć kopiec Fibonaciego (np. z C++ std::priority_queue)
// na wejście - graf
// na wyjście - obliczona ścieżka

extern TaskID actual_task_id;

void DijkstraSearchInit(){}

void vDijkstraSearchTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 6;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {
        actual_task_id = DIJKSTRA_SEARCH_ID;
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}