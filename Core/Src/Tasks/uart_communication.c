//
// Created by Łukasz Zelek on 17.03.2020.
//
//zawiera obustronną komunikację między robotem a innym urządzeniem po UART
//pomyśleć nad formatem przesyłanych paczek danych(Obecnie ASCII)
//rodzaje danych
/**
 OGRANICZENIA SPRZĘTOWE UART TX/RX
 25 kilobajtów w jedną stronę
 TX
 * DANE Z KALMANA X,Y,T,V,W,A 100Hz 24 bajty
 * DANE Z PID*, ZADANA + ODPOWIEDŹ 1kHz 24 bajty (możliwość obniżenia częstotliwości próbkowania)
 * MOZLIWOSC WCZYTANIA MAPY Z EEPROM (I INNYCH DANYCH TEZ) async
 * NAPIECIE NA BATERII 0.2Hz 1 bajt
 * DANE Z IMU 100Hz 14 bajtów
 * DANE Z LIDAR 30Hz 10 bajtów
 * WYLICZONA DECYZJA FLOODFILL async
 * WYLICZONA POZYCJA Z LANDMARK_DETECTION 100Hz 8 bajtów
 * WYLICZONE KOREKTY Z PATH_PLANNER 100Hz 8 bajtów
 * KOMUNIKATY TEKSTOWE, WARNINGI, BŁĘDY async
 RX
 * ZMIANA KLUCZOWYCH PARAMETRÓW NA EEPROM(stała), TYLKO NA POCZĄTKU DZIAŁANIA async
 * ZMIANA KLUCZOWYCH PARAMETRÓW W LOCIE W RAMIE(zapamiętana do resetu), KIEDYKOLWIEK async
 * zarządzanie zadaniami w RTOS
 * BEZPOŚREDNIE STEROWANIE Z PC 50Hz
 * */

#include <stdlib.h>
#include "Tasks/uart_communication.h"
#include "uart_dma.h"
#include "math_library.h"

extern TaskID actual_task_id;
extern UART_HandleTypeDef huart3;
extern char ParseBuffer[16];
extern UARTDMA_HandleTypeDef huartdma;
extern char TransmitBuffer[UART_SENDING_BUFFER_SIZE];
extern int transmitBufferSize;
extern int pending;
int pwm = 0;
int time_eight = 0;
int kalman_flag = 0;
int debug_flag = 1;
int bat_debug_flag = 0;
uint32_t eeprom_task_data = 0;
uint16_t eeprom_task_addr = 0x0;
char arg1;
uint8_t arg2;
uint8_t arg3;

void UARTCommunicationInit() {
    UARTDMA_Init(&huartdma, &huart3);
}

void vUARTReceiveTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 20;
    xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        ledDisable(2);
        if (UARTDMA_IsDataReady(&huartdma)) {
            ledEnable(2);
            UARTDMA_GetLineFromBuffer(&huartdma, ParseBuffer);
            if (strcmp(ParseBuffer, "A") == 0) {
                ledEnable(4);
            } else if (strcmp(ParseBuffer, "B") == 0) {
                ledDisable(4);
            } else if (strcmp(ParseBuffer, "0") == 0) {
                motorLeftSetPWM(0);
                motorRightSetPWM(0);
            }else if (strcmp(ParseBuffer, "Q") == 0) {
                //not printf in loops
                debug_flag = 0;
            }else if (strcmp(ParseBuffer, "E") == 0) {
                eepromMemoryDump(5);
                //eepromMemoryTestRW();
                //eepromFullMemoryErase();
                //eepromMemoryTestErase();
                //eepromMemoryDump(5);
            } else if (strcmp(ParseBuffer, "F") == 0) {
                motorLeftSetPWM(200);
                motorRightSetPWM(-100);
                time_eight = HAL_GetTick();
            } else if (strcmp(ParseBuffer, "TEST") == 0) {
#ifdef UNITY_TESTS_TYPE
                runAllTests();
#endif //TESTY JEDNOSTKOWE
            } else if (strcmp(ParseBuffer, "R") == 0) {
                HAL_NVIC_SystemReset();
            } else if (strcmp(ParseBuffer, "BAT") == 0) {
                bat_debug_flag = !bat_debug_flag;
            } else if (strcmp(ParseBuffer, "KALMAN") == 0) {
                motorLeftSetPWM(100);
                motorRightSetPWM(100);
                kalman_flag = 1;
            } else {
                if(ParseBuffer[0] == 'E') {
                    //format E,R/W,adress,dane_binarne\n
                    //E,W,13,0\n
                    sscanf(ParseBuffer,"E,%c,%d,%d\n",&arg1,&arg2,&arg3);
                    //printf("%c %d %d\n", arg1,arg2,arg3);
                    if(arg1 == 'R'){
                        uint32_t b = eepromRead4Bytes(eeprom_task_addr);
                        printf("%d\n",b);
                        printf("%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n",(b&1)>0,(b&2)>0,(b&4)>0,(b&8)>0,(b&16)>0,(b&32)>0,(b&64)>0,(b&128)>0,(b&256)>0,(b&512)>0,(b&1024)>0,(b&2048)>0,(b&4096)>0,(b&8192)>0);
                    }
                    else if(arg1 == 'W'){
                        eeprom_task_data = eepromRead4Bytes(0x0000);
                        if((eeprom_task_data&(1<<arg2)))eeprom_task_data -= (1<<arg2);
                        eeprom_task_data = eeprom_task_data | (arg3 << arg2);
                        HAL_Delay(EEPROM_I2C_TIMEOUT);
                        eepromSave4Bytes(&eeprom_task_addr, &eeprom_task_data);
                        HAL_Delay(EEPROM_I2C_TIMEOUT);
                        uint32_t b = eepromRead4Bytes(eeprom_task_addr);
                        printf("%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n",(b&1)>0,(b&2)>0,(b&4)>0,(b&8)>0,(b&16)>0,(b&32)>0,(b&64)>0,(b&128)>0,(b&256)>0,(b&512)>0,(b&1024)>0,(b&2048)>0,(b&4096)>0,(b&8192)>0);
                    }else if(arg1 == 'E'){
                        eepromFullMemoryErase();
                    }else if(arg1 == 'D'){
                        eeprom_task_data = 7173;
                        eepromSave4Bytes(&eeprom_task_addr, &eeprom_task_data);
                    }
                    else if(arg1 == 'I'){
                        printf("0-batlvl,1-belfor,2-btnctl,3-dijkst,4-distac\n");
                        printf("5-eeprom,6-flofil,7-imuact,8-kalfil,9-lamadt\n");
                        printf("10-pidctl,11-uarttx,12-uartrx,13-pathpl\n");
                    }
                }
                else{
                    if(ParseBuffer[0] >= 48 && ParseBuffer[0] < 58)
                    {
                        int numer = atoi(ParseBuffer);
                        int left_direction = 1, right_direction = 1;
                        if (numer > 1e8) {
                            numer -= 1e8;
                            left_direction = -1;
                        }
                        if (numer >= 1e7) {
                            numer -= 1e7;
                            right_direction = -1;
                        }
                        int leftPwm = numer / 1000;
                        int rightPwm = numer % 1000;
                        motorLeftSetPWM(left_direction * leftPwm);
                        motorRightSetPWM(right_direction * rightPwm);
                        printf("Start motors\n");
                    } else
                    {
                        printf("Unknown command %s\n", ParseBuffer);
                    }
                }
            }
        }
        actual_task_id = UART_RECEIVE_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}

void vUARTTransmitTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    // FIXME Warning UART Transmit DMA
    // Zbyt duża częstotliwość tutaj w połączeniu z korzystaniem z I2C powoduje błędy krytyczne
    const TickType_t xFrequency = 10;
    xLastWakeTime = xTaskGetTickCount();
    for (;;) {
        ledDisable(3);
        if (huart3.gState == HAL_UART_STATE_READY)
            if (transmitBufferSize > 0) {
                ledEnable(3);
                HAL_UART_Transmit_DMA(&huart3, TransmitBuffer, transmitBufferSize);
                transmitBufferSize = 0;
            }
        actual_task_id = UART_TRANSMIT_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}