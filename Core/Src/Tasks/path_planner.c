//
// Created by Łukasz Zelek on 23.05.2020.
//

//docelowo będzie zawierać regulator PID najwyższego poziomu dostosowujący prędkość robota do konkretnej ścieżki
//z odchyłki na pozycjach [delta x, delta y] przejdziemy na odchyłkę prędkości robota [delta v, delta omega] jako zadaną dla regulatorów niższego poziomu
//zadaniem tej części kodu jest też zaplanowanie trasy w oparciu o estymowaną najkrótszą trasę, obliczoną z A* (FAST RUN)
//możliwie prosta ścieżka podczas mapowania, wykluczająca zaklinowanie się robota
// na wejście przydaje się stan robota z kalmana [x,y,alfa,v,omega]
// na wyjście dostajemy obliczone [vErr,omegaErr]

#include "path_planner.h"
extern TaskID actual_task_id;
void PathPlannerInit()
{

}

void vPathPlannerTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 10;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {

        actual_task_id = PATH_PLANNER_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}