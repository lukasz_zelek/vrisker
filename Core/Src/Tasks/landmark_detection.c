//
// Created by Łukasz Zelek on 17.03.2020.
//
// zawiera kod lokalizujący robota w labiryncie z użyciem punktów stałych, (znamy punkty stykania się ścianek)
// na wejście przydaje się pozycja z kalmana [x,y]
// na wyjście dostajemy obliczone [x,y], okazjonalnie mapę otoczenia ściankami
//#include <stdlib.h>
#include "Tasks/landmark_detection.h"
////#include "math.h"
//#include "stdio.h"

//typedef enum{false=0,true}bool;

extern TaskID actual_task_id;

extern int8_t LD[2];
/**
 * TODO BIBLIOTEKA MATEMATYCZNA I FPU
 * Instrukcja wykrywania landmarków do ustalenia pozycji przy każdym obrocie robota
 *
 * 1) śledzimy pomiary i odrzucamy wszystkie poza 1-2000 <- stanowią one błędy
 * 2) odkładamy dane w buforze, kolejka, lub stos
 * 3) przeprowadzamy sortowanie kątowe
 * 4)
    Przekształcenie punktów z układu lidarów di na układ robota Pi,
    gdzie (0,0) oś robota

    P1 = (-d1, 0) + (-310, 230)
    P2 = (-d2 * sqrt(2) / 2, d2 * sqrt(2) / 2) + (-180, 330)
    P3 = (0, d3) + (0, 380)
    P4 = (d4 * sqrt(2) / 2, d4 * sqrt(2) / 2) + (180, 330)
    P5 = (d5, 0) + (310, 230)

    Wyprowadzony wzór na przekszałcenie przy układach współrzędnych,
    z układu robota(x,y) na układ świata(px,py)
    przy zmianie pozycji robota o r i kąta o alpha

    px = sqrt(x^2 + y^2) * cos(arctg(y/x) - alpha) - r * cos(alpha)
    py = sqrt(x^2 + y^2) * sin(arctg(y/x) - alpha) - r * sin(alpha)
 *
 * 5) Regresja liniowa od prawej do lewej badanie współliniowości
 * 6) Sprawdzamy punkty przecięcia odnalezionych prostych, to nasze landmarki,
 * update position to known
 * Jazda prosto:
 * 1) Jedziemy, po prostej
 * 2) Widzimy nagły spadek na jednej z diagonali, to nasz landmark,
 * przeprowadzamy update pozycji
 * 3) Oczekujemy na potwierdzenie przez czujniki boczne spadku,
 * również przeprowadzamy update pozycji
 * 4) Widzimy nagły przyrost i póżniejszy spadek, wykryty słupek,
 * aktualizujemy pozycję, to samo na bocznym
 * Jazda na ukos:
 * Wykrywamy słupki i analogiczie aktualizujemy pozycję
*/

uint16_t pastToFData[1000];
//



//outer definition
//#define N_START_MEASUREMENT_CYCLES 10
//#define N_SENSORS 5
//
//#define N_WALL_POINTS 1000
//
//#define ASSUMED_DISTANCE_BETWEEN_WALLS 168 /*mm*/
//
//
//void transform_from_sensors_to_robot_coordinates(int *sensor, int *data_x, int *data_y);
//int cramer(int x1, int y1, int x2, int y2, int *a, int *b);
//
//// to init
//int start_alpha;
//int start_distance_beetween_walls;
//int start_pos_x, start_pos_y;
//
//int pos_x, pos_y;
//
//int measure_avg[N_SENSORS];
//
//int x_buffer[N_WALL_POINTS]; // world coord
//int y_buffer[N_WALL_POINTS];
//
//int counter = 0;
//
//int x_data[N_SENSORS];
//int y_data[N_SENSORS];
//
//// defincja prostej w programie
//// prosta k (A,B,C)
//
//// zbior prostych (1, 0, 0), (0, 1, 0) jednostka mm
//// ogolnie proste mają postać (1, 0, A), (0, 1, B)
//int a1,a2,b1,b2,result; // temp values
//
//void init_start_position(int *sensor_data){
//    for(int i = 0; i < N_START_MEASUREMENT_CYCLES*N_SENSORS; i++)
//    {
//        measure_avg[i % N_SENSORS] += sensor_data[i];
//    }
//    for(int i = 0; i < N_SENSORS; i++)
//        measure_avg[i] /= N_START_MEASUREMENT_CYCLES;
//    transform_from_sensors_to_robot_coordinates(measure_avg, x_data, y_data);
//    result = cramer(x_data[0],y_data[0],x_data[1],y_data[1],&a1,&b1);
//    if(result != 0)printf("Cramer problem\n");
//    result = cramer(x_data[2],y_data[2],x_data[3],y_data[3],&a2,&b2);
//    if(result != 0)printf("Cramer problem\n");
//
//    start_alpha = 90 - atan(a1); // deg to rad
//    start_distance_beetween_walls = (abs(x_data[0])+abs(x_data[4]))*cos(start_alpha);
//
//    start_pos_x = (int)((-1.f*b1 + 1.f*b2 )/(1.f*a1 - 1.f*a2));
//    start_pos_y = a1*start_pos_x+b1;
//
//// twierdzenie cosinusow, rownanie prostej prostopadlej, trygonometria, długosc odcinka
//    int world_pos_x = (0 - start_pos_x)-sqrt(2)*sqrt((pow(start_pos_x,2)+pow(start_pos_y,2))*(1-cos(180+start_alpha)))*cos(atan(-start_pos_x/start_pos_y));
//    int world_pos_y = (0 - start_pos_y)-sqrt(2)*sqrt((pow(start_pos_x,2)+pow(start_pos_y,2))*(1-cos(180+start_alpha)))*sin(atan(-start_pos_x/start_pos_y));
///*
//Wyprowadzony wzór na przekszałcenie przy układach współrzędnych,
//z układu robota(x,y) na układ świata(px,py)
//przy zmianie pozycji robota o r i kąta o alpha
//// px = sqrt(x^2 + y^2) * cos(arctg(y/x) - alpha) - r * cos(alpha)
//// py = sqrt(x^2 + y^2) * sin(arctg(y/x) - alpha) - r * sin(alpha)
//*/
//
//}
//
//// used to check data from sensors before appending it in sensor_data table
//bool check_if_measurements_ok(int *sensor)
//{
//    for(int i = 0; i < N_SENSORS; i++)
//    {
//        if(sensor[i] > 2000 || sensor[i] < 1)
//            return false;
//    }
//
//    return true;
//}
///*
// * robot axis is (0,0)
// * measurements from left to right
// **/
////     P1 = (-d1, 0) + (-310, 230)
////     P2 = (-d2 * sqrt(2) / 2, d2 * sqrt(2) / 2) + (-180, 330)
////     P3 = (0, d3) + (0, 380)
////     P4 = (d4 * sqrt(2) / 2, d4 * sqrt(2) / 2) + (180, 330)
////     P5 = (d5, 0) + (310, 230)
//// const values below inherit from measurements on real robot
//void transform_from_sensors_to_robot_coordinates(int *sensor, int *data_x, int *data_y){
//    data_x[0] = -sensor[0] - 310;
//    data_y[0] = 230;
//    data_x[1] = -sensor[1]*sqrt(2)/2 - 180;
//    data_y[1] = sensor[1]*sqrt(2)/2 + 330;
//    data_x[2] = 0;
//    data_y[2] = sensor[2] + 380;
//    data_x[3] = sensor[3]*sqrt(2)/2 + 180;
//    data_y[3] = sensor[3]*sqrt(2)/2 + 330;
//    data_x[4] = sensor[4] + 310;
//    data_y[4] = 230;
//}
//
//int cramer(int x1, int y1, int x2, int y2, int *a, int *b){
//    float W = (x1 - x2) *1.f;
//    if(W < 0.0001)
//        return 1;
//    float Wa = (y1 - y2) *1.f;
//    float Wb = (x1*y2 - x2*y1) *1.f;
//    *a = (int)(Wa/W);
//    *b = (int)(Wb/W);
//    return 0;
//}
//
//void append_one_cycle_points(int *data_x, int *data_y, int data_size){
//    for(int i = 0; i < data_size; i++)
//    {
//        x_buffer[counter] = data_x[i];
//        y_buffer[counter] = data_y[i];
//        counter = (counter + 1) % N_WALL_POINTS;
//    }
//}
//
//void angle_sort_of_points(){
//
//}


void LandmarkDetectionInit()
{

}

void vLandmarkDetectionTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 33;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {

        actual_task_id = LANDMARK_DETECTION_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}
