//
// Created by Łukasz Zelek on 17.03.2020.
//
// algorytm brute-force znajdowania najkrótszej ścieżki w grafie O(n^3)
// docelowo nie używany w praktyce ze względu na dużą złożoność
// natomiast przydatny do testowania innych algorytmów jak A* czy Dijkstra
// implementację A* można umieścić tutaj lub w basic_algo
// na wejście - graf
// na wyjście - obliczona ścieżka
#include "Tasks/bellman_ford.h"



extern uint8_t actual_task_id;

void BellmanFordInit()
{
    
}

void vBellmanFordTask(void *pvParameters) {

    for(;;) {
        actual_task_id = BELLMAN_FORD_ID;
        vTaskDelay(500 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}
