//
// Created by Łukasz Zelek on 17.03.2020.
//
//poniższy kod monitoruje stan baterii

#include "battery_level.h"
#include "stdio.h"
#include "uart_dma.h"

uint32_t raw_voltage_adc;
extern float battery_voltage;
extern uint8_t actual_task_id;
extern int bat_debug_flag;
int tests_started = 0;

void BatteryLevelInit()
{
    raw_voltage_adc = BATTERY_LEVEL_ADC_INITIAL_VALUE;
    runBatteryLevelADC(&raw_voltage_adc);
}

void vBatteryLevelTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 1000;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {
#ifdef UNITY_TESTS_TYPE
        if(!tests_started)
        {
            tests_started = 1;
            runAllTests();
        }
#endif //TESTY JEDNOSTKOWE
        battery_voltage = (float)((raw_voltage_adc)/490.196f);
        if(raw_voltage_adc < BATTERY_LOW_VOLTAGE_THRESHOLD)
        {
            ledEnable(PWR_LED);
        }
        if(raw_voltage_adc < BATTERY_CRITICAL_VOLTAGE_THRESHOLD)
        {
            performLowVoltageAction();
            while (1) {
                ledToggle(5);
                HAL_Delay(1000);
                ledToggle(5);
                HAL_Delay(50);
            }
        }
        runBatteryLevelADC(&raw_voltage_adc);
        if(bat_debug_flag)printf("%d.%02d V\n",(int)battery_voltage, (int)(battery_voltage*100) % 100);
        actual_task_id = BATTERY_LEVEL_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}


