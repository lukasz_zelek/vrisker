//
/**
  ******************************************************************************
  * @file           : kalman_filer.c
  * @brief          : Unscented Kalman Filter Implementation
  ******************************************************************************
  * @attention
  * This code causes epilepsy.
  ******************************************************************************
  */
// Created by Łukasz Zelek on 17.03.2020.
//
// kod filtru Kalmana (UKF) prognozującego [x,y,alfa, v,omega] dla landmark_detection[x,y], path planner[x,y,alfa], pid_control[v,omega]

#include <Basics/encoders_odometry.h>
#include "Tasks/kalman_filter.h"
/* biblioteka matematyczna, funkcje trygonometryczne, operacje na macierzach */
#include "math_library.h"
/* wypisywanie danych po UART */
#include "uart_dma.h"
/* dane z akcelerometru (oś x)*/
#define accel_meas_x (receivedMPUData[0])
#define accel_meas_y (receivedMPUData[1])
#define accel_meas_z (receivedMPUData[2])
/* dane z lidaru (środkowy czujnik)*/
//#define lidar_meas DIST[2]
/* krok czasowy 10ms */
#define dt (0.01f)
/* pomiar prędkości liniowej robota z enkoderów */
int32_t velocity = 0;
int32_t left_cnt = 0;
int32_t right_cnt = 0;
int32_t left_speed = 0;
int32_t right_speed = 0;
//int32_t omega = 0;
float32_t time_scale_factor = 1.f;
/* pomiary z akcelerometru i żyroskopu, po kolei: acc_x, acc_y, acc_z, temp, gyro_x, gyro_y, gyro_z */
extern int16_t receivedMPUData[7];
/* ID zadania systemowego RTOS */
extern TaskID actual_task_id;
/* pomiary z lidaru*/
//extern uint16_t DIST[5];
// todo odchylenia standardowe czujnikow
const float lidar_sdev_perc = 0.04f; // odchylenie standardowe pomiaru odległości z lidaru %/100
const float enc_sdev = 10.045f; // odchylenie standardowe pomiaru prędkości z enkoderów w mm/s
const float accel_sdev = 30.28f; // odchylenie standardowe pomiaru przyspieszenia z akcelerometru w mm/s^2
const float omega_sdev = 1; // odchylenie standardowe pomiaru prędkości kątowej z enkoderów w deg/s
const float bias_sdev = 0.1f; // odchylenie standardowe pomiaru biasu prędkości kątowej z gyro w deg/s
const float gyro_sdev = 1; // odchylenie standardowe pomiaru prędkości kątowej z gyro w deg/s
const float alpha_sdev = 1; // odchylenie standardowe pomiaru kąta z lidaru w deg todo ()
// todo pomiary zrobic i potwierdzic wartosci
const float Rlidar = (1000 * lidar_sdev_perc) * (1000 * lidar_sdev_perc);
const float Rodo = enc_sdev * enc_sdev;
const float Rgyro = gyro_sdev * gyro_sdev;

//Zmienna globalna - przemieszczenie
int przemieszczenie = 0;

/* WSKAŹNIKI do macierzy trwalych */
matrix *f,*h,*P,*Q,*R,*x,*z,*Wm,*Wc,*A,*Y,*X,*X2,*m1,*Y1,*P1,*Z1,*z1,*Z2,*S,*C,*K;

/**
 * @brief Jeden krok pętli filtru Kalmana (etap predykcji oraz korekcji).
 * @param acceleration pomiar z akcelerometru, służy jako wejście UKF.
 */
void unscented_kalman_filter(float32_t acceleration) {
    matrix *tmp1, *tmp2, *tmp3, *tmp4, *tmp5, *tmp6, *tmp7;
    int l = x->mat->numRows;
    float32_t alpha = 1e-3;//fixme tunable
    float32_t kappa = 0;//fixme tunable
    float32_t beta = 2;//fixme tunable
    float32_t lambda = alpha * alpha * ((float32_t)l + kappa) - (float32_t)beta;//fixme Projekt_UKF.pdf Małysiak, Winkler, Wojtyś, Zelek
    float32_t c = (float32_t)l + lambda;
    Wm->mat->pData[0] = lambda / c;Wm->mat->pData[1] = 0.5f / c;Wm->mat->pData[2] = 0.5f / c;Wm->mat->pData[3] = 0.5f / c;Wm->mat->pData[4] = 0.5f / c;
    Wm->mat->pData[5] = 0.5f / c;Wm->mat->pData[6] = 0.5f / c;
    Wc->mat->pData[0] = lambda / c;Wc->mat->pData[1] = 0.5f / c;Wc->mat->pData[2] = 0.5f / c;Wc->mat->pData[3] = 0.5f / c;Wc->mat->pData[4] = 0.5f / c;
    Wc->mat->pData[5] = 0.5f / c;Wc->mat->pData[6] = 0.5f / c;
    Wc->mat->pData[0]=Wc->mat->pData[0] + (1 - alpha * alpha + beta);
    c = sqrtf(c);
    A = matsca(matcho(P),c);
    Y = matdup(x,3);
    tmp1 = matadd(Y,A);
    tmp2 = matsub(Y,A);
    X = matmer(matmer(x,tmp1),tmp2);
    memset(m1->mat->pData,0,(m1->mat->numCols) * (m1->mat->numRows));//zerowanie m1 [brak tego powodował ciężki do wykrycia błąd]
    for(int k = 0; k < 7; k++)
    {
        tmp3 = matmul(f,matfra(X,0,X->mat->numRows,k,k+1));
        tmp3->mat->pData[2] = acceleration;/*błąd wynikał ze złego zapisania równań stanu*/
        X2->mat->pData[k] = tmp3->mat->pData[0];
        X2->mat->pData[7+k] = tmp3->mat->pData[1];//TODO matinj
        X2->mat->pData[14+k] = tmp3->mat->pData[2];
        m1 = matadd(m1,matsca(tmp3,Wm->mat->pData[k]));
    }
    tmp4 = matdup(m1,7);
    Y1 = matsub(X2,tmp4);
    tmp5 = matdia(Wc);
    P1 = matadd(matmul(matmul(Y1,tmp5),mattra(Y1)),Q);
    memset(z1->mat->pData,0,(z1->mat->numCols)*(z1->mat->numRows));//zerowanie z1 [brak tego powodował ciężki do wykrycia błąd]
    for(int k = 0; k < 7; k++)
    {
        tmp6 = matmul(h,matfra(X2,0,X2->mat->numRows,k,k+1));
        Z1->mat->pData[k] = tmp6->mat->pData[0];
        Z1->mat->pData[7+k] = tmp6->mat->pData[1];//TODO matinj
        z1 = matadd(z1,matsca(tmp6,Wm->mat->pData[k]));
    }
    tmp7 = matdup(z1,7);
    Z2 = matsub(Z1,tmp7);
    S = matadd(matmul(matmul(Z2,tmp5),mattra(Z2)),R);//S = Z2 * diag(Wc) * Z2.T + R;
    C = matmul(matmul(Y1,tmp5),mattra(Z2));//C = Y1 * diag(Wc) * Z2.T;
    K=matmul(C,matinv(S));//K = C * inv (S);
    x=matadd(m1,matmul(K,matsub(z,z1)));//m = m1 + K * (zmat - z1);
    P=matsub(P1,matmul(K,mattra(C)));//P = P1 - K * C.T;
}

/**
 * @brief Funkcja licząca prędkość robota na podstawie zliczeń timer-ów.
 */
void performEncodersMeasurements() {
    uint32_t m = micros();
    left_cnt = getLeftTickCount();
    right_cnt = getRightTickCount();
    resetLeftTickCount();
    resetRightTickCount();
    micros_start();
    int32_t time_factor = (1000000 / (m * (int)time_scale_factor));
    left_speed = time_factor * (left_cnt);
    right_speed = time_factor * (right_cnt);
    velocity = (left_speed + right_speed) / 2;
    //omega = (right_speed - left_speed) / L_DISTANCE_BETWEEN_WHEEL;
}

/**
 * @brief Funkcja inicjalizująca macierze i parametry filtru.
 */
void KalmanFilterInit() {
    encodersInit();
//    f = matini(7,7,'f');
//    h = matini(4, 7, 'h');
//    P = matini(7, 7, 'P');
//    Q = matini(7, 7, 'Q');
//    R = matini(4, 4, 'R');
//    x = matini(7, 1, 'x');
//    z = matini(4, 1, 'z');
//    Wm = matini(1, 15, 'W');
//    Wc = matini(1, 15, 'W');
//    A = matini(7, 7, 'A');
//    Y = matini(7, 7, 'Y');
//    X = matini(7, 15, 'X');
//    X2 = matini(7, 15, 'X');
//    m1 = matini(7, 1, 'm');
//    Y1 = matini(7, 15, 'Y');
//    P1 = matini(7, 7, 'P');
//    Z1 = matini(4, 15, 'Z');
//    z1 = matini(4, 1, 'z');
//    Z2 = matini(4, 15, 'Z');
//    S = matini(4, 4, 'S');
//    C = matini(7, 4, 'C');
//    K = matini(7, 4, 'K');
//    f->mat->pData[0] = 1;
//    f->mat->pData[1] = dt;
//    f->mat->pData[2] = dt*dt/2;
//    f->mat->pData[4] = 1;
//    f->mat->pData[5] = dt;
//    h->mat->pData[0] = 1;
//    h->mat->pData[4] = 1;
//    P->mat->pData[0] = (100 * lidar_sdev_perc)*(100 * lidar_sdev_perc);
//    P->mat->pData[4] = enc_sdev*enc_sdev/100.f;
//    P->mat->pData[8] = accel_sdev*accel_sdev/100.f;
//    Q->mat->pData[0] = Rlidar * dt * dt / 2;
//    Q->mat->pData[4] = Rodo * dt;
//    Q->mat->pData[8] =  (accel_sdev * accel_sdev);
//    R->mat->pData[0] = Rlidar;
//    R->mat->pData[3] = Rodo;
//    x->mat->pData[2] = 20;
    time_scale_factor = ENCODER_TICKS_PER_FULL_WHEEL_ROTATION / ( 29 * PI);
}
extern int kalman_flag;
extern int debug_flag;
///fixme zalecenia
/**
 *stosować w cyklach po 5 sekund, potem następują błędy numeryczne przekraczające 1mm
 * jedno wykonanie 140us
 * */

/**
 * @brief Funkcja wykonująca się z częstotliwością 100 Hz.
 * @param pvParameters opcjonalne parametry wątku RTOSa.
 */
void vKalmanFilterTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 10;
    xLastWakeTime = xTaskGetTickCount();
    float32_t a;
    for (;;) {
        performEncodersMeasurements();
        // pomiary z czujników
        //f->mat->pData[8] = accel_meas/(1.67f);
        //z->mat->pData[0] = 600 - lidar_meas;
//        z->mat->pData[0] = lidar_x;
//        z->mat->pData[1] = lidar_y;
//        z->mat->pData[2] = enc_v;
//        z->mat->pData[3] = enc_omega;
        // theoretically it works
        //a = sqrtf(powf(accel_meas_x*ACCELEROMETER_SCALE,2)+powf(accel_meas_y*ACCELEROMETER_SCALE,2)+powf(accel_meas_z*ACCELEROMETER_SCALE,2))-GRAVITY_ACCELERATION;
        a = accel_meas_y*ACCELEROMETER_SCALE;
        //unscented_kalman_filter(a);
        //przemieszczenie = x->mat->pData[0];
        //printf("$%d %d %d %d %d %d;\n",(int) x->mat->pData[0], (int) x->mat->pData[1], (int)x->mat->pData[2],(int) z->mat->pData[0], (int) z->mat->pData[1], (int) a);//Serial plotter
        //printf("%d %d %d %d %d %d\n",(int) x->mat->pData[0], (int) x->mat->pData[1], (int)x->mat->pData[2],(int) z->mat->pData[0], (int) z->mat->pData[1], (int) a);//Arduino
        //if(debug_flag)printf("%d,%d,%d,%d,%d\n",(int) x->mat->pData[0], (int) x->mat->pData[1], (int)x->mat->pData[2],(int) z->mat->pData[0], (int) z->mat->pData[1]);//Telemetry Viewer
        //matfre();
        actual_task_id = KALMAN_FILTER_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}
