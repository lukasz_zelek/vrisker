//
// Created by Łukasz Zelek on 17.03.2020.
//
// poniższy kod umożliwia odczyt w terminalu danych z LIDAR, służy do DEBUGU

#include "Tasks/distance_sensors.h"

#define R VL53L0X_ContinuousRequest

extern TaskID actual_task_id;
extern uint16_t DIST[5];
extern int TOF_DATA_READ[5];
extern int debug_flag;

/**
 * @brief  Initialization function for VL53L0X ToF Sensors.
 */
void DistanceSensorsInit()
{
    VL53L0X_Init();
}

/**
 * @brief  RTOS Task for printing distance values.
 */
void vDistanceSensorsTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 33;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {
        if(debug_flag)printf("%d %d %d %d %d\n", DIST[0], DIST[1], DIST[2], DIST[3], DIST[4]);
        actual_task_id = DISTANCE_SENSORS_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}
