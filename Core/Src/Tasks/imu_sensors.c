//
// Created by Łukasz Zelek on 17.03.2020.
//
// poniższy kod umożliwia odczyt w terminalu danych z IMU, służy do DEBUGU

#include "Tasks/imu_sensors.h"
#include "mpu6050_i2c.h"
#include "uart_dma.h"
#define M receivedMPUData

extern TaskID actual_task_id;
extern I2C_HandleTypeDef hi2c1;
extern int16_t receivedMPUData[7];
extern uint16_t DIST[5];
extern uint32_t omega;
extern uint32_t velocity;
int dmaI2CDataReceived = TRUE;
int dmaI2CDataTransmited = TRUE;
extern int8_t LD[2]; //lidar data
//zebrać pomiary skrzynki - kontenera
//real pomiary, lx, ly, v,    omega,     ax, ay, az, gx, gy, gz
//jednostka     cm  cm  cm/s  stopnie/10s  dm/s^2      stopnie/10s
//presc         10                       167            164
//pozostałe pomiary scianki, temperatura, adc voltage,
//rozne ksztalty jedz w srodku
// {,theta,omega,v,}
uint8_t BD[10];
extern int debug_flag;
void IMUSensorsInit()
{
    IMU_MPU6050_Init();
}

void vIMUSensorsTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    const TickType_t xFrequency = 10;
    xLastWakeTime = xTaskGetTickCount();
    for(;;) {
        //printf("$%i %i %i %i %i %i %i;\n", M[0]/167,M[1]/167,M[2]/167,M[4]/164,M[5]/164,M[6]/164);
        if(debug_flag)printf("%i %i %i %i %i %i %i\n", M[0]/167,M[1]/167,M[2]/167,M[3],M[4]/164,M[5]/164,M[6]/164);
        actual_task_id = IMU_SENSORS_ID;
        vTaskDelayUntil(&xLastWakeTime, xFrequency);
    }
    vTaskDelete(NULL);
}

