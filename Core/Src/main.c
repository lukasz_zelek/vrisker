/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "battery_level.h"
#include "buttons_control.h"
#include "distance_sensors.h" /// todo wykrywanie scian lidar, position detection
#include "imu_sensors.h" /// todo dane do estymacji
#include "flood_fill.h"/// todo rowiazywanie labiryntu
#include "bellman_ford.h"/// todo pomoc do rozwiazania labiryntu
#include "dijkstra_search.h"/// todo pomoc do rozwiazania labiryntu
#include "kalman_filter.h"/// todo estymacja na krotkich odcinkach
#include "landmark_detection.h"/// todo gps + orientacja + kalibracja
#include "eeprom_memory.h"/// todo zapis trwaly do pamieci
#include "pid_control.h"/// todo regulator pid idealnie sterujący robotem
#include "path_planner.h"
#include "uart_communication.h"
#include "uart_dma.h"
#include "basic_algo.h"
#include "mpu6050_i2c.h"
#include "semphr.h"
#include "tests_config.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
TaskID actual_task_id = 0xff;
volatile uint64_t time_micros_start = 0;
int mpuint_soft = TRUE;
int mpu_is_initialized = FALSE;
uint8_t start_task_list[14];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
static void prvSetupHardware(void);
void vApplicationIdleHook(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    //DWT -> CTRL |= (1 << 0); // enable DWTCYCCNT
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC3_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM12_Init();
  MX_USART3_UART_Init();
  MX_TIM14_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */

    prvSetupHardware();/// todo zastanowic się nad wykorzystaniem Distortos, który jest napisany w C++
    //SEGGER_SYSVIEW_Conf();
    //SEGGER_SYSVIEW_Start();
    /// FIXME TASK KRYTYCZNY, URUCHAMIANY OBLIGATORYJNIE, chroni baterię przeed przypadkowym rozładowaniem
    if(start_task_list[BATTERY_LEVEL_ID])xTaskCreate(vBatteryLevelTask, "batlvl", 1000, NULL, 1, NULL); //1 NULL -> DANE WEJSCOWE
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1) {
        CppMain();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
static void prvSetupHardware(void) {
    EEPROMMemoryInit();
    TaskManagerInit(eepromRead4Bytes(0));
    if(start_task_list[BATTERY_LEVEL_ID])BatteryLevelInit();
    ledOutputInit();
    HAL_TIM_Base_Start_IT(&htim6);
    HAL_TIM_Base_Start(&htim7);
    if(start_task_list[BUTTONS_CONTROL_ID])ButtonsControlInit();
    int welcome_blinks = 6;
    while (welcome_blinks-- > 0) {
        ledToggle(1);
        ledToggle(2);
        ledToggle(3);
        ledToggle(4);
        HAL_Delay(200);
    }
    if(start_task_list[UART_RECEIVE_ID]||start_task_list[UART_TRANSMIT_ID])UARTCommunicationInit();
    printf("MICROMOUSE VRISKER by KN INTEGRA\n\n");
    if(start_task_list[DISTANCE_SENSORS_ID])DistanceSensorsInit();
    if(start_task_list[IMU_SENSORS_ID])IMUSensorsInit();
    if(start_task_list[PID_CONTROL_ID])PIDControlInit();
    if(start_task_list[KALMAN_FILTER_ID])KalmanFilterInit();
    if(start_task_list[EEPROM_MEMORY_ID])EEPROMMemoryInit();
    if(start_task_list[BELLMAN_FORD_ID])BellmanFordInit();
    if(start_task_list[DIJKSTRA_SEARCH_ID])DijkstraSearchInit();
    if(start_task_list[FLOOD_FILL_ID])FloodFillInit();
    if(start_task_list[LANDMARK_DETECTION_ID])LandmarkDetectionInit();
    if(start_task_list[PATH_PLANNER_ID])PathPlannerInit();
    ledEnable(1);
}

void vApplicationIdleHook(void) {
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */
}

uint16_t micros() {
    return __HAL_TIM_GET_COUNTER(&htim7);
}

void micros_start() {
    __HAL_TIM_SET_COUNTER(&htim7, 0);
}

void create_tasks(void) {
    if(start_task_list[BUTTONS_CONTROL_ID])xTaskCreate(vButtonsControlTask, "btnctl", 100, NULL, 1, NULL);/// TODO 2 NULL UCHWYT POZWALAJACY NA EDYCJE
    if(start_task_list[BELLMAN_FORD_ID])xTaskCreate(vBellmanFordTask, "belfor", 1000, NULL, 1, NULL );//acyclic // TODO MAZE SEARCH
    if(start_task_list[DIJKSTRA_SEARCH_ID])xTaskCreate(vDijkstraSearchTask, "dijkst", 1000, NULL, 1, NULL );//acyclic
    if(start_task_list[DISTANCE_SENSORS_ID])xTaskCreate(vDistanceSensorsTask, "distac", 1000, NULL, 1, NULL );//cyclic INTERRUPT //TODO DMA
    if(start_task_list[EEPROM_MEMORY_ID])xTaskCreate(vEEPROMMemoryTask, "eeprom", 1000, NULL, 1, NULL );//acyclic //TODO I2C DMA // TODO ODCZYT PRZY STARCIE, ZAPIS CENNYCH INFORMACJI
    if(start_task_list[FLOOD_FILL_ID])xTaskCreate(vFloodFillTask, "flofil", 1000, NULL, 1, NULL );//acyclic
    if(start_task_list[IMU_SENSORS_ID])xTaskCreate(vIMUSensorsTask, "imuact", 1000, NULL, 1, NULL);;//cyclic INTERRUPT //TODO I2C DMA, USING MPU INT DMP
    if(start_task_list[KALMAN_FILTER_ID])xTaskCreate(vKalmanFilterTask, "kalfil", 6000, NULL, 1, NULL);;//cyclic 1 1000Hz // TODO FILTR KALMANA WYLICZAJACY NA BIEZOCA ESTYMATE POZYCJI ACCEL GYRO ENCODER TOF // macierze zajmują łącznie około 1600 bajtów, stąd duży rozmiar stosu
    if(start_task_list[LANDMARK_DETECTION_ID])xTaskCreate(vLandmarkDetectionTask, "lamadt", 1000, NULL, 1, NULL );;//cyclic 2 //TODO WYKRYCIE // TODO FILTR DOLNOPRZEPUSTOWY + TRANSFORMATA FOURIERA Z EKSTRAKCJA CECH + AKTUALIZACJA POLOZENIA NA MAPIE
    if(start_task_list[PATH_PLANNER_ID])xTaskCreate(vPathPlannerTask, "pathpl", 1000, NULL, 1, NULL);
    if(start_task_list[PID_CONTROL_ID])xTaskCreate(vPIDControlTask, "pidctl", 1000, NULL, 1, NULL);//cyclic 1 1000Hz
    if(start_task_list[UART_RECEIVE_ID])xTaskCreate(vUARTReceiveTask, "uartrx", 1000, NULL, 1, NULL);
    if(start_task_list[UART_TRANSMIT_ID])xTaskCreate(vUARTTransmitTask, "uarttx", 1000, NULL, 1, NULL);
}

void start_rtos(void) {
    vTaskStartScheduler();
}

int runAllTests(void)
{
#ifdef UNITY_TESTS_TYPE
    printf("Unity Tests RUNNING\n");
    UnityBegin("tests/vrisker.elf");
    RUN_TEST(addFuntion_Test_CheckIfProperResponse);
    RUN_TEST(multiplicationFunction_Test_CheckIfProperResponse);
    RUN_TEST(sortFunction_Test_Sort);
    return UNITY_END();
#endif //TESTY JEDNOSTKOWE
    return 0;
}
extern int debug_flag;
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
    else if (htim->Instance == TIM6) {
        // frequency 1kHz PWM
        generateLEDPWMCallback();
        if (mpu_is_initialized) {
            mpuint_soft = !mpuint_soft;//
            // frequency 0.5kHz INT
            if (mpuint_soft&&debug_flag)MPUInterruptHandler();
        }
    }
  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    while (1) {
        ledToggle(1);
        HAL_Delay(500);
    }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
