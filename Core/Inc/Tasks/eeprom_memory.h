//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_EEPROM_MEMORY_H
#define VRISKER_EEPROM_MEMORY_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "eeprom_i2c.h"
#define EEPROM_MEMORY_ID 5

void EEPROMMemoryInit();
void vEEPROMMemoryTask(void *pvParameters);
#endif //VRISKER_EEPROM_MEMORY_H
