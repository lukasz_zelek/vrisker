//
// Created by Łukasz Zelek on 24.03.2020.
//

#ifndef VRISKER_BUTTONS_CONTROL_H
#define VRISKER_BUTTONS_CONTROL_H

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "buttons_input.h"

#define BUTTONS_CONTROL_ID 2

void ButtonsControlInit();

void vButtonsControlTask(void *pvParameters);

#endif //VRISKER_BUTTONS_CONTROL_H
