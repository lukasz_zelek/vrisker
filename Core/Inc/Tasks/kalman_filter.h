//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_KALMAN_FILTER_H
#define VRISKER_KALMAN_FILTER_H

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define KALMAN_FILTER_ID 8
//biegun – 9,83332
//normalne – 9,80665
//równik – 9,78030
//Gdańsk – 9,8145
//Warszawa – 9,8123
//Kraków – 9,8105
//Katowice – 9,8101
//Wrocław – 9,8115
#define GRAVITY_ACCELERATION (9810.5f)
#define ACCELEROMETER_SCALE (0.000061f*GRAVITY_ACCELERATION)

void KalmanFilterInit();
void vKalmanFilterTask(void *pvParameters);

#endif //VRISKER_KALMAN_FILTER_H
