//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_LANDMARK_DETECTION_H
#define VRISKER_LANDMARK_DETECTION_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define LANDMARK_DETECTION_ID 9

void LandmarkDetectionInit();
void vLandmarkDetectionTask(void *pvParameters);
#endif //VRISKER_LANDMARK_DETECTION_H
