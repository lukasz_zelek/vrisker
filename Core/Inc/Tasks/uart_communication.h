//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_UART_COMMUNICATION_H
#define VRISKER_UART_COMMUNICATION_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "led_output.h"
#include "motors_pwm.h"
#include "eeprom_i2c.h"

#define UART_RECEIVE_ID 11
#define UART_TRANSMIT_ID 12

void UARTCommunicationInit();
void vUARTReceiveTask(void *pvParameters);
void vUARTTransmitTask(void *pvParameters);
#endif //VRISKER_UART_COMMUNICATION_H
