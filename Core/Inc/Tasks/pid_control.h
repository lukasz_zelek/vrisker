//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_PID_CONTROL_H
#define VRISKER_PID_CONTROL_H

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "math.h"

#define PID_CONTROL_ID 10

void PIDControlInit();
void vPIDControlTask(void *pvParameters);

#endif //VRISKER_PID_CONTROL_H
