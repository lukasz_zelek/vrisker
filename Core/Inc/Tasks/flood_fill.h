//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_FLOOD_FILL_H
#define VRISKER_FLOOD_FILL_H

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define FLOOD_FILL_ID 6

void FloodFillInit();
void vFloodFillTask(void *pvParameters);

#endif //VRISKER_FLOOD_FILL_H
