//
// Created by Łukasz Zelek on 23.05.2020.
//

#ifndef VRISKER_PATH_PLANNER_H
#define VRISKER_PATH_PLANNER_H

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define PATH_PLANNER_ID 13

void PathPlannerInit();
void vPathPlannerTask(void *pvParameters);

#endif //VRISKER_PATH_PLANNER_H
