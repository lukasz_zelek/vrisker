//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_IMU_SENSORS_H
#define VRISKER_IMU_SENSORS_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "math.h"

#define IMU_SENSORS_ID 7

void IMUSensorsInit();
void vIMUSensorsTask(void *pvParameters);
#endif //VRISKER_IMU_SENSORS_H
