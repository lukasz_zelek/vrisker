//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_DISTANCE_SENSORS_H
#define VRISKER_DISTANCE_SENSORS_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "vl53l0x_i2c.h"

#define DISTANCE_SENSORS_ID 4

void DistanceSensorsInit();
void vDistanceSensorsTask(void *pvParameters);
#endif //VRISKER_DISTANCE_SENSORS_H
