//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_DIJKSTA_SEARCH_HPP
#define VRISKER_DIJKSTA_SEARCH_HPP
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define DIJKSTRA_SEARCH_ID 3

void DijkstraSearchInit();
void vDijkstraSearchTask(void *pvParameters);
#endif //VRISKER_DIJKSTA_SEARCH_HPP
