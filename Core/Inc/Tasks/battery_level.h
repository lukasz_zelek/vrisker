//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_BATTERY_HPP
#define VRISKER_BATTERY_HPP

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "battery_adc.h"
#include "led_output.h"

#define BATTERY_LEVEL_ID 0
#define BATTERY_LOW_VOLTAGE_THRESHOLD 3480
#define BATTERY_CRITICAL_VOLTAGE_THRESHOLD 3000
#define BATTERY_LEVEL_ADC_INITIAL_VALUE ((1<<12)-1)
#define BATLVL_TASK_DELAY 1000
#define PWR_LED 5

void BatteryLevelInit();
void vBatteryLevelTask(void *pvParameters);

#endif //VRISKER_BATTERY_HPP
