//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_BELLMAN_FORD_H
#define VRISKER_BELLMAN_FORD_H
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define BELLMAN_FORD_ID 1

void BellmanFordInit();
void vBellmanFordTask(void *pvParameters);

#endif //VRISKER_BELLMAN_FORD_H
