//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_BATTERY_ADC_H
#define VRISKER_BATTERY_ADC_H
#include "main.h"
void runBatteryLevelADC(uint32_t *variable);
void performLowVoltageAction();
#endif //VRISKER_BATTERY_ADC_H
