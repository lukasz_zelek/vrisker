//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_MOTORS_PWM_H
#define VRISKER_MOTORS_PWM_H
#include "main.h"
void motorsPWMInit();
void motorLeftSetPWM(int32_t duty);
void motorRightSetPWM(int32_t duty);
int motorLeftGetPWM();
int motorRightGetPWM();
#endif //VRISKER_MOTORS_PWM_H
