// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_MATH_LIBRARY_H
#define VRISKER_MATH_LIBRARY_H
#include "main.h"
#include "arm_math.h"
#include "math.h"

typedef struct{
    arm_matrix_instance_f32 *mat;
    char friendly_name;
}matrix;

matrix* matini(int n, int m, char c); /* matrix initialization */
matrix* matinv(matrix* A); /* matrix inversion */
matrix* mattra(matrix* A); /* matrix transposition */
matrix* matsca(matrix* A, float32_t s); /* matrix scale */
matrix* matmul(matrix* A, matrix* B); /* matrix multiplication */
matrix* matadd(matrix* A, matrix* B); /* matrix addition */
matrix* matsub(matrix* A, matrix* B); /* matrix subtraction */
matrix* matcho(matrix *A); /* matrix cholesky decomposition */
matrix* matfra(matrix* A, int n_start, int n_end, int m_start, int m_end); /* matrix fragment */
matrix* matdia(matrix* v); /* matrix diagonal */
matrix* matdup(matrix* v, int k); /* matrix duplication */
matrix* matmer(matrix* A, matrix* B); /* matrix merge */
void matprt(matrix* A); /* matrix print */
void matfre(void); /* matrix free allocated memory by library */
void matinj(matrix* A, int n_start, int n_end, int m_start, int m_end, matrix *v); /* matrix inject */

#endif //VRISKER_MATH_LIBRARY_H
