//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_LED_OUTPUT_H
#define VRISKER_LED_OUTPUT_H
#include "main.h"
#define LED1_OUTPUT_PWM 50
#define LED2_OUTPUT_PWM 50
#define LED3_OUTPUT_PWM 50
#define LED4_OUTPUT_PWM 50
#define LED5_OUTPUT_PWM 500
void ledOutputInit();
void ledToggle(uint16_t ledx);
void ledEnable(uint16_t ledx);
void ledDisable(uint16_t ledx);
void generateLEDPWMCallback(void);
#endif //VRISKER_LED_OUTPUT_H
