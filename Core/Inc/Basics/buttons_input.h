//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_BUTTONS_INPUT_H
#define VRISKER_BUTTONS_INPUT_H
#include "main.h"
GPIO_PinState BTN_IsPressed(uint8_t btnid);
#endif //VRISKER_BUTTONS_INPUT_H
