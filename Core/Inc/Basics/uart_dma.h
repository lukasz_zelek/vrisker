//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_UART_DMA_H
#define VRISKER_UART_DMA_H
#include "main.h"
#include "stdio.h"
#include "string.h"
#include "led_output.h"

#define DMA_RX_BUFFER_SIZE 64
#define UART_RECEIVING_BUFFER_SIZE 256
#define DMA_TX_BUFFER_SIZE 128
#define UART_SENDING_BUFFER_SIZE 512

typedef struct
{
    UART_HandleTypeDef* huart; // UART handler

    uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE]; // DMA direct buffer
    uint8_t DMA_TX_Buffer[DMA_TX_BUFFER_SIZE]; // DMA direct buffer
    uint8_t UART_RX_Buffer[UART_RECEIVING_BUFFER_SIZE]; // UART working circular buffer


    uint16_t UartBufferHead;
    uint16_t UartBufferTail;
    uint8_t UartBufferLines;
}UARTDMA_HandleTypeDef;

void UARTDMA_Init(UARTDMA_HandleTypeDef *huartdma, UART_HandleTypeDef *huart);

void UARTDMA_UartIrqHandler(UARTDMA_HandleTypeDef *huartdma);
void UARTDMA_DmaIrqHandler(UARTDMA_HandleTypeDef *huartdma);
uint8_t UARTDMA_IsDataReady(UARTDMA_HandleTypeDef *huartdma);
int UARTDMA_GetLineFromBuffer(UARTDMA_HandleTypeDef *huartdma, char *OutBuffer);




#endif //VRISKER_UART_DMA_H
