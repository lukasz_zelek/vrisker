//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_MPU6050_I2C_H
#define VRISKER_MPU6050_I2C_H
#include "main.h"
#include <mpu6050.h>

#define I2C_TIMEOUT 100
#define DELAY_IMU_CYCLE_WRITE 1
#define DELAY_IMU_CYCLE_READ 2

void IMU_MPU6050_Init(void);
int getMPUDataReadyStatus();
void setMPUDataReadyStatus(int enable);
void MPU6050_Write(uint8_t slaveAddr, uint8_t regAddr, uint8_t data);
void IMU_MPU6050_ReadRaw(int16_t *buff);
void MPUInterruptHandler(void);

#endif //VRISKER_MPU6050_I2C_H
