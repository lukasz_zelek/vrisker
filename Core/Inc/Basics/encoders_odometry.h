//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_ENCODERS_ODOMETRY_H
#define VRISKER_ENCODERS_ODOMETRY_H
#include "main.h"

#define L_DISTANCE_BETWEEN_WHEEL (10)
#define ENCODER_TICKS_PER_FULL_WHEEL_ROTATION 600

void encodersInit();
//uint16_t getLeftTickCount();
//uint16_t getRightTickCount();
int32_t getLeftTickCount();
int32_t getRightTickCount();
void resetLeftTickCount();
void resetRightTickCount();
#endif //VRISKER_ENCODERS_ODOMETRY_H
