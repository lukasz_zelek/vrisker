//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_VL53L0X_I2C_H
#define VRISKER_VL53L0X_I2C_H
#include "main.h"
#include "vl53l0x_api.h"

void VL53L0X_Init();

uint16_t VL53L0X_ContinuousRequest(int);

void VL53L0X_ResetAllSensors();
void VL53L0X_SensorTurnOn(int);
void VL53L0X_InitContinuous(int);
void VL53L0X_DisableInterrupts();
void VL53L0X_EnableInterrupts();
void VL53L0X_PerformMeasurement(int sensor);

VL53L0X_Error rangingTest(VL53L0X_Dev_t *pMyDevice);

#endif //VRISKER_VL53L0X_I2C_H
