//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_EEPROM_I2C_H
#define VRISKER_EEPROM_I2C_H
#include "main.h"

#define DELAY_EEPROM_CYCLE_WRITE 2
#define DELAY_EEPROM_CYCLE_READ 2
#define EEPROM_I2C_TIMEOUT 20
#define EEPROM_DEV_ADDRESS 0xa0

void eepromMemoryDump(uint32_t timeout);

void eepromFullMemoryErase();

uint32_t eepromRead4Bytes(uint16_t addr);

void eepromSave4Bytes(uint16_t *addr, uint32_t *data);

void eepromMemoryTestRW();

void eepromMemoryTestErase();
#endif //VRISKER_EEPROM_I2C_H
