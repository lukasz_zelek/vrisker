//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_TASK_MANAGER_HPP
#define VRISKER_TASK_MANAGER_HPP

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define TASK_MANAGER_ID 14
void TaskManagerInit(uint32_t task_list);
void vTaskManagerTask(void *pvParameters);


#endif //VRISKER_TASK_MANAGER_HPP
