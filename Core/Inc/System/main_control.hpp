//
// Created by Łukasz Zelek on 17.03.2020.
//

#ifndef VRISKER_MAIN_CONTROL_HPP
#define VRISKER_MAIN_CONTROL_HPP

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

#define MAIN_CONTROL_ID 15

void MainControlInit();
void vMainControlTask(void *pvParameters);

#endif //VRISKER_MAIN_CONTROL_HPP
