import threading
import serial
import keyboard
import os
from time import sleep

connected = False
port = 'COM5'
baud = 115200

clear = lambda: os.system('cls')

serial_port = serial.Serial(port, baud, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS, timeout=0)

keyboard.add_hotkey('ctrl + l', clear)
# for killing process ctrl-break
# FIXME napisać apkę do skanowania COM ports z numerami i nazwami urządzeń

print("Serial port opened")

def handle_data(data):
    if len(data) != 0:
        print(data, end='')


def read_from_port(ser):
    global connected
    while not connected:
        #serin = ser.read()
        connected = True

        while True:
            if ser.inWaiting() > 0:  # if incoming bytes are waiting to be read from the serial input buffer
                data_str = ser.read(ser.inWaiting()).decode('ascii')
                # read the bytes and convert from binary array to ASCII
                # Put the rest of your code you want here
                handle_data(data_str)
            sleep(0.01)


thread = threading.Thread(target=read_from_port, args=(serial_port,))
thread.start()

while True:
    ciag_znakow = input("")

    ciag_znakow = "{0:s}".format(ciag_znakow)
    sleep(0.01)
    serial_port.write(str.encode(ciag_znakow+"\n"))
