//
// Created by Łukasz Zelek on 15.07.2020.
//
#include "unity.h"
#include "vrisker_tests.h"
#include "main.h"

volatile uint8_t value = 0;

void setUp(void)
{
    value = 1;
    //printf("\nsetUpStart\n");
}

void addFuntion_Test_CheckIfProperResponse(void)
{
    TEST_ASSERT_EQUAL(8, 3+5);
    TEST_ASSERT_EQUAL(1e2, (int)(pow(8,2))+(int)(pow(6,2)));
}

void multiplicationFunction_Test_CheckIfProperResponse(void)
{
    TEST_ASSERT_EQUAL(4, 2*2);
    TEST_ASSERT_EQUAL(64, 8*8);
}

int a_tab[5] = {5,2,1,4,3};
int b_tab[5] = {1,2,3,4,5};

void sortFunction_Test_Sort(void){
    sort_using_std(a_tab, 5);
    TEST_ASSERT_EQUAL_INT_ARRAY(b_tab,a_tab,5);
}

void tearDown(void)
{
    value = 0;
    //printf("\nsetUpTearDown\n");
}