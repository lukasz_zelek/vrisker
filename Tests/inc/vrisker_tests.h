//
// Created by Łukasz Zelek on 15.07.2020.
//

#ifndef VRISKER_VRISKER_TESTS_H
#define VRISKER_VRISKER_TESTS_H

#include "unity.h"

void addFuntion_Test_CheckIfProperResponse(void);
void multiplicationFunction_Test_CheckIfProperResponse(void);
void sortFunction_Test_Sort(void);

#endif //VRISKER_VRISKER_TESTS_H
